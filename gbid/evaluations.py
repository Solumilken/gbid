import numpy as np


def root_mean_squared_error(y_true, y_pred):
    return np.sqrt(np.square(y_pred - y_true).mean())


def root_mean_squared_logarithmic_error(y_true, y_pred):
    return np.sqrt(np.square(np.log1p(y_pred) - np.log1p(y_true)).mean())


def summary(metrics, y_true, y_pred, label_column,
            result_prefix="", y_pred_proba=None):
    """y_pred, y_pred_proba, y_true should be numpy array"""
    if label_column == "log1p_label":
        y_true = np.expm1(y_true)
    result_dict = {}
    if len(y_true) == len(y_pred):
        for metric, alias in metrics.items():
            metric_func = globals()[metric]
            result_dict[result_prefix + alias] = metric_func(y_true, y_pred)
    else:
        raise ValueError("length is not correct!!!!")
    return result_dict


def main():
    import pprint
    metrics = ["root_mean_squared_error", "root_mean_squared_logarithmic_error"]
    y_true = np.array([1, 4, 6])
    y_pred = np.array([1, 2, 3])
    result = summary(metrics, y_true, y_pred)
    pprint.pprint(result, indent=1, width=80, depth=None)

if __name__ == '__main__':
    main()
