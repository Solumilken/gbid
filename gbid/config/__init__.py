from os.path import join, splitext, basename

import yaml


def _load_from_yaml(config):
    """Load configuration from YAML file and compose full the configuration

    Args:
        config: file path string or stream
    """
    if isinstance(config, str):
        with open(config) as fp:
            yml_data = yaml.load(fp)
    else:
        yml_data = yaml.load(config)
    return yml_data


class PathConfig(object):

    def __init__(self, root_data_dir, feature_output_dir, model_output_dir,
                 prediction_output_dir, result_output_dir, raw_data_dir,
                 train_transaction_path,
                 test_transaction_path, client_table_path,
                 client_table_extension_path, product_table_path,
                 product_table_extension_path, sales_depot_table_path,
                 data_hdf_path):
        self.root_data_dir = root_data_dir
        self.feature_output_dir = join(root_data_dir, feature_output_dir)
        self.model_output_dir = join(root_data_dir, model_output_dir)
        self.prediction_output_dir = join(root_data_dir, prediction_output_dir)
        self.result_output_dir = join(root_data_dir, result_output_dir)
        self.raw_data_dir = join(root_data_dir, raw_data_dir)

        self.train_transaction_path = join(self.raw_data_dir,
                                           train_transaction_path)
        self.test_transaction_path = join(self.raw_data_dir,
                                          test_transaction_path)
        self.client_table_path = join(self.raw_data_dir,
                                      client_table_path)
        self.product_table_path = join(self.raw_data_dir,
                                       product_table_path)
        self.sales_depot_table_path = join(self.raw_data_dir,
                                           sales_depot_table_path)

        self.client_table_extension_path = join(self.root_data_dir,
                                                client_table_extension_path)
        self.product_table_extension_path = join(self.root_data_dir,
                                                 product_table_extension_path)
        self.data_hdf_path = join(self.root_data_dir, data_hdf_path)

    @classmethod
    def load_from_yaml(cls, config):
        yml_data = _load_from_yaml(config)
        return cls(**yml_data)


class FeatureConfig(object):

    def __init__(self, feature_output_dir, label_column, extra_columns,
                 feature_list, feature_hdf_store_path, concat_feature_hdf_path,
                 sampled_feature_output_path):
        self.label_column = label_column
        self.extra_columns = extra_columns
        self.feature_list = feature_list
        self.feature_hdf_store_path = join(feature_output_dir,
                                           feature_hdf_store_path)
        self.concat_feature_hdf_path = join(feature_output_dir,
                                            concat_feature_hdf_path)
        self.sampled_feature_output_path = join(feature_output_dir,
                                                sampled_feature_output_path)
        self.config_name = splitext(basename(self.concat_feature_hdf_path))[0]

    @classmethod
    def load_from_yaml(cls, feature_output_dir, config):
        yml_data = _load_from_yaml(config)
        return cls(feature_output_dir, **yml_data)


class TrainConfig(object):

    def __init__(self, model_config_path, subtrain_weeks, valid_weeks,
                 train_weeks, test_weeks, evaluation_metrics,
                 validation_output_excel_columns,
                 testing_output_excel_columns,
                 ensemble_output_excel_columns):
        self.subtrain_weeks = subtrain_weeks
        self.valid_weeks = valid_weeks
        self.train_weeks = train_weeks
        self.test_weeks = test_weeks
        self.model_config_path = model_config_path
        self.evaluation_metrics = evaluation_metrics
        self.validation_output_excel_columns = validation_output_excel_columns
        self.testing_output_excel_columns = testing_output_excel_columns
        self.ensemble_output_excel_columns = ensemble_output_excel_columns

    @classmethod
    def load_from_yaml(cls, model_config_path, config):
        yml_data = _load_from_yaml(config)
        return cls(model_config_path, **yml_data)


class Config(object):

    def __init__(self, path_config, feature_config=None, train_config=None,
                 model_config=None):
        self.path = PathConfig.load_from_yaml(path_config)
        if feature_config is not None:
            self.feature = FeatureConfig.load_from_yaml(
                self.path.feature_output_dir, feature_config)
        if train_config is not None:
            self.train = TrainConfig.load_from_yaml(model_config, train_config)


if __name__ == '__main__':
    config = Config("configs/en_path_config.yml",
                    "configs/feature1_config.yml",
                    "configs/train_config.yml",
                    "configs/model_config.yml")
    import pprint
    pprint.pprint(config.path.__dict__, indent=1, width=80, depth=None)
    pprint.pprint(config.feature.__dict__, indent=1, width=80, depth=None)
    pprint.pprint(config.train.__dict__, indent=1, width=80, depth=None)
