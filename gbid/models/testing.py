from os.path import join

import pprint
import gc
import pandas as pd
import numpy as np

from .models import (
    load_data,
    train_with_parameters,
    save_model,
    write_prediction,
    gbid_predict,
)
from ..evaluations import summary
from ..utils import IterTimer, mkdir_p


def load_test_data(config):
    train_X_df, test_X_df, train_y_df, test_y_df = load_data(
        config.feature.concat_feature_hdf_path,
        config.feature.label_column,
        config.feature.feature_list,
        config.train.train_weeks,
        config.train.test_weeks)

    train_X = np.ascontiguousarray(train_X_df.values)
    test_X = np.ascontiguousarray(test_X_df.values)
    train_y = np.ascontiguousarray(train_y_df.values)
    test_y = np.ascontiguousarray(test_y_df.values)
    test_id = test_y_df.index

    train_X_df = None
    test_X_df = None
    train_y_df = None
    test_y_df = None
    gc.collect()

    return train_X, test_X, train_y, test_y, test_id


def write_test_prediction(process_started_at, test_id, test_pred, test_i,
                          model_class_name, model_params_name, config):
    mkdir_p(config.path.prediction_output_dir)
    output_filename = "test_{}_{}_{:02}_{}_{}.csv".format(
        config.feature.config_name,
        process_started_at.strftime("%Y-%m-%dT%H:%M:%S"),
        test_i, model_class_name, model_params_name)
    output_path = join(config.path.prediction_output_dir, output_filename)
    write_prediction(test_id, test_pred, output_path)


def test(config, process_started_at, excel_writer):
    train_X, test_X, train_y, test_y, test_id = load_test_data(config)

    print("train_X shape: {}".format(train_X.shape))
    print("test_X shape : {}".format(test_X.shape))
    print("train_y shape: {}".format(train_y.shape))
    print("test_y shape : {}".format(test_y.shape))

    label_column = config.feature.label_column
    result_df = pd.DataFrame()
    for test_i, (model_class_name, model_params_name, model) in enumerate(
            train_with_parameters(config.train.model_config_path,
                                  train_X, train_y)):
        model_filename = save_model(
            process_started_at, test_i, model_class_name, model_params_name,
            model, config, prefix="test_")

        train_pred = gbid_predict(train_X, model, label_column)
        train_summary = summary(config.train.evaluation_metrics,
                                train_y, train_pred, label_column, "train_")
        test_pred = gbid_predict(test_X, model, label_column)

        result_dict = {
            'model': model_class_name,
            'params': model_params_name,
            'model_filename': model_filename,
            **train_summary,
        }
        print("result_dict:")
        pprint.pprint(result_dict, indent=1, width=80, depth=None)

        result_df = result_df.append(result_dict, ignore_index=True)  # pylint: disable=R0204

        gc.collect()

        with IterTimer("Writing to " + excel_writer.path):
            result_df[config.train.testing_output_excel_columns].to_excel(
                excel_writer, "test_result")
            excel_writer.save()

        write_test_prediction(process_started_at, test_id, test_pred, test_i,
                              model_class_name, model_params_name, config)

    return result_df
