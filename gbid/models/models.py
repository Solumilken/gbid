import gc
from os.path import join
import itertools
import traceback
import pprint
from datetime import datetime

import ipdb
import pandas as pd
import numpy as np
import yaml

from ..utils import IterTimer, mkdir_p, save_pkl


def load_data(concat_feature_hdf_path, label_column, feature_list,
              train_weeks, test_weeks, scale=False):
    gc.collect()
    with IterTimer("Loading data from " + concat_feature_hdf_path):
        data_df = pd.read_hdf(concat_feature_hdf_path, 'df')

    train_idx = data_df['week_number'].isin(train_weeks)
    test_idx = data_df['week_number'].isin(test_weeks)

    if scale:
        with IterTimer("Calculating mean/std for scaling"):
            mean = data_df.loc[data_df['week_number'] > 3, feature_list].mean()
            std = data_df.loc[data_df['week_number']
                              > 3, feature_list].std(ddof=0)
    else:
        mean = 0
        std = 1

    with IterTimer("Train/test splitting and scaling"):
        train_X_df = (data_df.loc[train_idx, feature_list] - mean) / std
        train_y_df = data_df.loc[train_idx, label_column]
        test_X_df = (data_df.loc[test_idx, feature_list] - mean) / std
        test_y_df = data_df.loc[test_idx, label_column]

    return train_X_df, test_X_df, train_y_df, test_y_df


def try_load_yaml(yaml_path):
    while True:
        try:
            with open(yaml_path) as fp:
                data = yaml.load(fp)
            break
        except:  # pylint: disable=W0702
            traceback.print_exc()
            # reload or go into ipdb if there's error
            while True:
                is_try_again = input("Error while loading schedule.yml, "
                                     "try again ([y]/n)? ")
                if is_try_again == "n":
                    ipdb.set_trace()
                elif is_try_again == "y" or is_try_again == "":
                    break
    return data


def train_single_model(model_class_name, params, X, y):
    # pylint: disable=unused-variable
    from sklearn.linear_model import Ridge
    from sklearn.ensemble import (
        RandomForestRegressor,
        GradientBoostingRegressor,
    )
    from .xgboost_with_warm_start import XGBRegressorWithWarmStart as XGBRegressor
    from .ensemble import UniformBlending

    model_class = locals()[model_class_name]
    model = model_class(**params)
    with IterTimer("Training"):
        model.fit(X, y)
    return model


def train_single_model_with_warm_start(model, model_params, X, y):
    model.set_params(n_estimators=model_params['n_estimators'],
                     warm_start=True)
    with IterTimer("Training"):
        model.fit(X, y)
    return model


WARM_START_MODEL_LIST = [
    'GradientBoostingRegressor',
    'XGBRegressor',
]


def can_warm_start(prev_model_class_name, model_class_name,
                   prev_model_params, model_params):
    if (prev_model_class_name == model_class_name
            and model_class_name in WARM_START_MODEL_LIST):
        if prev_model_params['n_estimators'] >= model_params['n_estimators']:
            return False
        for key, value in prev_model_params.items():
            if key == 'n_estimators':
                continue
            if key not in model_params:
                return False
            if model_params[key] != value:
                return False
        return True
    return False


def train_with_parameters(model_config_path, X, y):
    prev_model_class_name = None
    prev_model_params = None
    prev_model = None
    for model_i in itertools.count():
        for params_i in itertools.count():
            # load config
            model_config = try_load_yaml(model_config_path)
            if (model_i >= len(model_config)
                    or params_i >= len(model_config[model_i]['model_params'])):
                break
            print("\033[1;32m[model {}, parameters {}]\033[m [{}]"
                  .format(model_i, params_i, datetime.now()))
            model_class_name = model_config[model_i]['model']
            print("model_class_name: " + model_class_name)
            model_params = model_config[model_i]['model_params'][params_i]
            print("model_params: ")
            pprint.pprint(model_params)
            model_params_name = (model_config[model_i]['model_params_formatter']
                                 .format(**model_params))

            # train
            if can_warm_start(prev_model_class_name, model_class_name,
                              prev_model_params, model_params):
                print("warm start!")
                model = train_single_model_with_warm_start(
                    prev_model, model_params, X, y)
            else:
                model = train_single_model(
                    model_class_name, model_params, X, y)
            gc.collect()
            yield (model_class_name, model_params_name, model)

            prev_model_class_name = model_class_name
            prev_model_params = model_params
            prev_model = model

        if model_i >= len(model_config):
            break


def gbid_predict(X, model, label_column):
    with IterTimer("Predicting"):
        pred = model.predict(X).clip(0, np.inf)
        if label_column == "log1p_label":
            pred = np.expm1(pred)
    return pred


def save_model(process_started_at, test_i, model_class_name, model_params_name,
               model, config, prefix=""):
    mkdir_p(config.path.model_output_dir)
    output_filename = "{}{}_{}_{:02}_{}_{}.pkl".format(
        prefix, config.feature.config_name,
        process_started_at.strftime("%Y-%m-%dT%H:%M:%S"),
        test_i, model_class_name, model_params_name)
    output_path = join(config.path.model_output_dir, output_filename)
    save_pkl(model, output_path, verbose=3)
    return output_filename


def write_prediction(data_id, pred, output_path):
    with IterTimer("Writing prediction to " + output_path):
        pred_df = pd.DataFrame({'Demanda_uni_equil': pred},
                               index=data_id)
        pred_df.index.rename("id", inplace=True)
        pred_df.to_csv(output_path, float_format="%.8g")
