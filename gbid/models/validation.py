import pprint
import gc
import pandas as pd
import numpy as np

from .models import load_data, train_with_parameters, save_model, gbid_predict
from ..evaluations import summary
from ..utils import IterTimer


def load_validation_data(config):
    subtrain_X_df, valid_X_df, subtrain_y_df, valid_y_df = load_data(
        config.feature.concat_feature_hdf_path,
        config.feature.label_column,
        config.feature.feature_list,
        config.train.subtrain_weeks,
        config.train.valid_weeks)

    subtrain_X = np.ascontiguousarray(subtrain_X_df.values)
    valid_X = np.ascontiguousarray(valid_X_df.values)
    subtrain_y = np.ascontiguousarray(subtrain_y_df.values)
    valid_y = np.ascontiguousarray(valid_y_df.values)

    subtrain_X_df = None
    valid_X_df = None
    subtrain_y_df = None
    valid_y_df = None
    gc.collect()
    return subtrain_X, valid_X, subtrain_y, valid_y


def validate(config, process_started_at, excel_writer):
    subtrain_X, valid_X, subtrain_y, valid_y = load_validation_data(config)

    print("subtrain_X shape: {}".format(subtrain_X.shape))
    print("valid_X shape   : {}".format(valid_X.shape))
    print("subtrain_y shape: {}".format(subtrain_y.shape))
    print("valid_y shape   : {}".format(valid_y.shape))

    label_column = config.feature.label_column
    result_df = pd.DataFrame()
    for valid_i, (model_class_name, model_params_name, model) in enumerate(
            train_with_parameters(config.train.model_config_path,
                                  subtrain_X, subtrain_y)):
        model_filename = save_model(
            process_started_at, valid_i, model_class_name, model_params_name,
            model, config, prefix="valid_")

        subtrain_pred = gbid_predict(subtrain_X, model, label_column)
        subtrain_summary = summary(config.train.evaluation_metrics, subtrain_y,
                                   subtrain_pred, label_column, "subtrain_")
        valid_pred = gbid_predict(valid_X, model, label_column)
        valid_summary = summary(config.train.evaluation_metrics,
                                valid_y, valid_pred, label_column, "valid_")
        result_dict = {
            'model': model_class_name,
            'params': model_params_name,
            'model_filename': model_filename,
            **subtrain_summary,
            **valid_summary,
        }
        print("result_dict:")
        pprint.pprint(result_dict, indent=1, width=80, depth=None)

        result_df = result_df.append(result_dict, ignore_index=True)  # pylint: disable=R0204

        with IterTimer("Writing to " + excel_writer.path):
            result_df[config.train.validation_output_excel_columns].to_excel(
                excel_writer, "validation_result")
            excel_writer.save()

    return result_df
