class UniformBlending(object):

    def fit(self, X, y):  # pylint: disable=W0613
        return self

    def predict(self, X):
        return X.mean(1)
