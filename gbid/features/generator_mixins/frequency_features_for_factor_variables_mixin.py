import numpy as np
import pandas as pd

from ..decorators import will_generate
from ..registered_decorators import (require_intermediate_data, features)


def _week_mean_frequency(transaction_df, groupby_item, index,
                         index_to_be_removed, feature_name,
                         last_number_weeks=9):
    week_number_reverse = list(reversed(range(2, 12)))
    cut_point = week_number_reverse[last_number_weeks]
    part_transaction_df = \
        transaction_df[
            transaction_df.index.get_level_values('Semana') > cut_point]
    frequency_s = \
        part_transaction_df.groupby(level=[groupby_item, 'Semana']).size()
    mean_frequency_s = frequency_s.groupby(level=groupby_item).mean()
    feature_s = \
        mean_frequency_s.reindex(index, level=groupby_item,
                                 copy=False, fill_value=mean_frequency_s.mean())
    feature_s.reset_index(level=index_to_be_removed, drop=True, inplace=True)
    feature_s.rename(feature_name, inplace=True)
    assert feature_s.isnull().sum() == 0, "there is NaN in this series"
    return feature_s

def _week_mean_frequency_fillna_fix(
        transaction_df, groupby_item, index, index_to_be_removed, 
        feature_name, fill_value, last_number_weeks=9):
    week_number_reverse = list(reversed(range(2, 12)))
    cut_point = week_number_reverse[last_number_weeks]
    part_transaction_df = \
        transaction_df[
            transaction_df.index.get_level_values('Semana') > cut_point]
    frequency_s = \
        part_transaction_df.groupby(level=[groupby_item, 'Semana']).size()
    mean_frequency_s = frequency_s.groupby(level=groupby_item).mean()
    feature_s = \
        mean_frequency_s.reindex(index, level=groupby_item,
                                 copy=False, fill_value=fill_value)
    feature_s.reset_index(level=index_to_be_removed, drop=True, inplace=True)
    feature_s.rename(feature_name, inplace=True)
    assert feature_s.isnull().sum() == 0, "there is NaN in this series"
    assert np.isinf(feature_s).sum() == 0, "there is Inf in this series"
    return feature_s

def _week_mean_frequency_fillna_mean_fix(
        transaction_df, groupby_item, index, index_to_be_removed, 
        feature_name, fill_value, last_number_weeks=9):
    week_number_reverse = list(reversed(range(2, 12)))
    cut_point = week_number_reverse[last_number_weeks]
    part_transaction_df = \
        transaction_df[
            transaction_df.index.get_level_values('Semana') > cut_point]
    frequency_s = \
        part_transaction_df.groupby(level=[groupby_item, 'Semana']).size()
    mean_frequency_s = frequency_s.groupby(level=groupby_item).mean()
    feature_s = \
        mean_frequency_s.reindex(index, level=groupby_item,
                                 copy=False, fill_value=mean_frequency_s.mean())
    feature_s.reset_index(level=index_to_be_removed, drop=True, inplace=True)
    feature_s.rename(feature_name, inplace=True)
    assert feature_s.isnull().sum() == 0, "there is NaN in this series"
    assert np.isinf(feature_s).sum() == 0, "there is Inf in this series"
    return feature_s

def _week_mean_frequency_fillna_median_fix(
        transaction_df, groupby_item, index, index_to_be_removed, 
        feature_name, last_number_weeks=9):
    week_number_reverse = list(reversed(range(2, 12)))
    cut_point = week_number_reverse[last_number_weeks]
    part_transaction_df = \
        transaction_df[
            transaction_df.index.get_level_values('Semana') > cut_point]
    frequency_s = \
        part_transaction_df.groupby(level=[groupby_item, 'Semana']).size()
    mean_frequency_s = frequency_s.groupby(level=groupby_item).mean()
    feature_s = \
        mean_frequency_s.reindex(index, level=groupby_item,
                                 copy=False, fill_value=mean_frequency_s.median())
    feature_s.reset_index(level=index_to_be_removed, drop=True, inplace=True)
    feature_s.rename(feature_name, inplace=True)
    assert feature_s.isnull().sum() == 0, "there is NaN in this series"
    assert np.isinf(feature_s).sum() == 0, "there is Inf in this series"
    return feature_s



class FrequencyFeaturesMixin(object):

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_client')
    def gen_week_mean_frequency_by_same_client(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                groupby_item='Cliente_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_client')
        return {'week_mean_frequency_by_same_client': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_product')
    def gen_week_mean_frequency_by_same_product(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                groupby_item='Producto_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_product')
        return {'week_mean_frequency_by_same_product': feature_s}


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_channel')
    def gen_week_mean_frequency_by_same_channel(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                groupby_item='Canal_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_channel')
        return {'week_mean_frequency_by_same_channel': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_depot')
    def gen_week_mean_frequency_by_same_depot(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                groupby_item='Agencia_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_depot')
        return {'week_mean_frequency_by_same_depot': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_route')
    def gen_week_mean_frequency_by_same_route(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                groupby_item='Ruta_SAK',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_route')
        return {'week_mean_frequency_by_same_route': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_client_in_last_three_weeks')
    def gen_week_mean_frequency_by_same_client_in_last_three_weeks(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                last_number_weeks=3,
                groupby_item='Cliente_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_client_in_last_three_weeks')
        return {'week_mean_frequency_by_same_client_in_last_three_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_product_in_last_three_weeks')
    def gen_week_mean_frequency_by_same_product_in_last_three_weeks(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                last_number_weeks=3,
                groupby_item='Producto_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_product_in_last_three_weeks')
        return {'week_mean_frequency_by_same_product_in_last_three_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_channel_in_last_three_weeks')
    def gen_week_mean_frequency_by_same_channel_in_last_three_weeks(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                last_number_weeks=3,
                groupby_item='Canal_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_channel_in_last_three_weeks')
        return {'week_mean_frequency_by_same_channel_in_last_three_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_depot_in_last_three_weeks')
    def gen_week_mean_frequency_by_same_depot_in_last_three_weeks(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                last_number_weeks=3,
                groupby_item='Agencia_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_depot_in_last_three_weeks')
        return {'week_mean_frequency_by_same_depot_in_last_three_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_route_in_last_three_weeks')
    def gen_week_mean_frequency_by_same_route_in_last_three_weeks(self, data):
        feature_s = \
            _week_mean_frequency(
                transaction_df=data['transaction'],
                last_number_weeks=3,
                groupby_item='Ruta_SAK',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_route_in_last_three_weeks')
        return {'week_mean_frequency_by_same_route_in_last_three_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate(['week_%s_mean_frequency_by_same_product' %num
                   for num in range(3,12)])
    def gen_single_week_mean_frequency_by_same_product(self, data):
        feature_dict = {}
        for week_number in range(3,12):
            part_transaction_df = \
                data['transaction'][
                    data['transaction'].index.get_level_values('Semana') == week_number]
            feature_name = 'week_' + str(week_number) + '_mean_frequency_by_same_product'
            feature_s = \
                _week_mean_frequency(
                    transaction_df=part_transaction_df,
                    groupby_item='Producto_ID',
                    index=data['index'],
                    index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                    feature_name=feature_name)
            feature_dict[feature_name] = feature_s
        return feature_dict

###############################################################################################

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_product_fillna_m1')
    def gen_week_mean_frequency_by_same_product_fillna_m1(self, data):
        feature_s = \
            _week_mean_frequency_fillna_fix(
                transaction_df=data['transaction'],
                groupby_item='Producto_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                fill_value=-1,
                feature_name='week_mean_frequency_by_same_product_fillna_m1')
        return {'week_mean_frequency_by_same_product_fillna_m1': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_product_in_last_three_weeks_fillna_0')
    def gen_week_mean_frequency_by_same_product_in_last_three_weeks_fillna_0(self, data):
        feature_s = \
            _week_mean_frequency_fillna_fix(
                transaction_df=data['transaction'],
                last_number_weeks=3,
                groupby_item='Producto_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                fill_value=0,
                feature_name='week_mean_frequency_by_same_product_in_last_three_weeks_fillna_0')

        return {'week_mean_frequency_by_same_product_in_last_three_weeks_fillna_0': feature_s}


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_product_in_last_three_weeks_fillna_m1')
    def gen_week_mean_frequency_by_same_product_in_last_three_weeks_fillna_m1(self, data):
        feature_s = \
            _week_mean_frequency_fillna_fix(
                transaction_df=data['transaction'],
                last_number_weeks=3,
                groupby_item='Producto_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                fill_value=-1,
                feature_name='week_mean_frequency_by_same_product_in_last_three_weeks_fillna_m1')

        return {'week_mean_frequency_by_same_product_in_last_three_weeks_fillna_m1': feature_s}


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('week_mean_frequency_by_same_product_in_last_three_weeks_fillna_median')
    def gen_week_mean_frequency_by_same_product_in_last_three_weeks_fillna_median(self, data):
        feature_s = \
            _week_mean_frequency_fillna_median_fix(
                transaction_df=data['transaction'],
                last_number_weeks=3,
                groupby_item='Producto_ID',
                index=data['index'],
                index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                feature_name='week_mean_frequency_by_same_product_in_last_three_weeks_fillna_median')

        return {'week_mean_frequency_by_same_product_in_last_three_weeks_fillna_median': feature_s}


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate(['week_%s_mean_frequency_by_same_product_fillna_m1' %num
                   for num in range(3,12)])
    def gen_single_week_mean_frequency_by_same_product_fillna_m1(self, data):
        feature_dict = {}
        for week_number in range(3,12):
            part_transaction_df = \
                data['transaction'][
                    data['transaction'].index.get_level_values('Semana') == week_number]
            feature_name = 'week_' + str(week_number) + '_mean_frequency_by_same_product_fillna_m1'
            feature_s = \
                _week_mean_frequency_fillna_fix(
                    transaction_df=part_transaction_df,
                    groupby_item='Producto_ID',
                    index=data['index'],
                    index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                    fill_value=-1,
                    feature_name=feature_name)
            feature_dict[feature_name] = feature_s
        return feature_dict

############################################################################################################
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate(['week_%s_mean_frequency_by_same_product_fillna_0' %num
                   for num in range(3,12)])
    def gen_single_week_mean_frequency_by_same_product_fillna_0(self, data):
        feature_dict = {}
        for week_number in range(3,12):
            part_transaction_df = \
                data['transaction'][
                    data['transaction'].index.get_level_values('Semana') == week_number]
            feature_name = 'week_' + str(week_number) + '_mean_frequency_by_same_product_fillna_0'
            feature_s = \
                _week_mean_frequency_fillna_fix(
                    transaction_df=part_transaction_df,
                    groupby_item='Producto_ID',
                    index=data['index'],
                    index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                    fill_value=0,
                    feature_name=feature_name)
            feature_dict[feature_name] = feature_s
        return feature_dict



    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate(['week_%s_mean_frequency_by_same_product_fillna_mean' %num
                   for num in range(3,12)])
    def gen_single_week_mean_frequency_by_same_product_fillna_mean(self, data):
        feature_dict = {}
        for week_number in range(3,12):
            part_transaction_df = \
                data['transaction'][
                    data['transaction'].index.get_level_values('Semana') == week_number]
            feature_name = 'week_' + str(week_number) + '_mean_frequency_by_same_product_fillna_mean'
            feature_s = \
                _week_mean_frequency_fillna_mean_fix(
                    transaction_df=part_transaction_df,
                    groupby_item='Producto_ID',
                    index=data['index'],
                    index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                    feature_name=feature_name)
            feature_dict[feature_name] = feature_s
        return feature_dict


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate(['week_%s_mean_frequency_by_same_product_fillna_median' %num
                   for num in range(3,12)])
    def gen_single_week_mean_frequency_by_same_product_fillna_median(self, data):
        feature_dict = {}
        for week_number in range(3,12):
            part_transaction_df = \
                data['transaction'][
                    data['transaction'].index.get_level_values('Semana') == week_number]
            feature_name = 'week_' + str(week_number) + '_mean_frequency_by_same_product_fillna_median'
            feature_s = \
                _week_mean_frequency_fillna_median_fix(
                    transaction_df=part_transaction_df,
                    groupby_item='Producto_ID',
                    index=data['index'],
                    index_to_be_removed=list(set(data['index'].names) - set(['id'])),
                    feature_name=feature_name)
            feature_dict[feature_name] = feature_s
        return feature_dict

#######################################################################################################
