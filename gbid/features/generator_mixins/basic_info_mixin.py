import numpy as np
import pandas as pd

from ..decorators import will_generate
from ..registered_decorators import (require_intermediate_data, features, intermediate_data)


def _object_encode(series):
    series_encode = series.copy()
    for code_num, uitem in enumerate(series.unique(), start=1):
        series_encode[series == uitem] = code_num
    return series_encode

class BasicInfoMixin(object):

    @features(skip_if_exist=True)
    @require_intermediate_data(['index'])
    @will_generate(['week_number', 'depot_id', 'channel_id', 'route_id',
                    'client_id', 'product_id'])
    def gen_basic_info(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        basic_info_df = \
            pd.DataFrame(index=data['index']).reset_index()[index_to_be_removed]
        basic_info_rename_dict = {
            'Semana': 'week_number', 'Agencia_ID': 'depot_id',
            'Canal_ID': 'channel_id', 'Ruta_SAK': 'route_id',
            'Cliente_ID': 'client_id', 'Producto_ID': 'product_id'
        }
        result_df = \
            pd.DataFrame(data=basic_info_df.values,
                         index=data['index'],
                         columns=[basic_info_rename_dict[name]
                                  for name in list(basic_info_df.columns)])
        result_df.reset_index(level=index_to_be_removed,
                              drop=True, inplace=True)
        assert np.sum(result_df.isnull().sum()) == 0, "basic info has NaN"
        return result_df

    #######################################
    # Generate features: product features #
    #######################################

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'producto_table'])
    @will_generate(['product_name', 'product_brand'])
    def gen_product_strinfo(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        product_info = ['product_name', 'product_brand']
        product_features = []
        for subinfo in product_info:
            product_strinfo_s = data['producto_table'][subinfo]
            product_strinfo_s = _object_encode(product_strinfo_s)
            product_strinfo_s = product_strinfo_s.astype(np.int32, copy=False)
            product_features.append(product_strinfo_s)
        full_df = pd.concat(product_features, axis=1)
        full_df = full_df.reindex(data['index'], level='Producto_ID',
                                  fill_value=0, copy=False)
        full_df.reset_index(level=index_to_be_removed, drop=True, inplace=True)
        return full_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'producto_table'])
    @will_generate('product_weight')
    def gen_product_weight(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        product_weight_s = data['producto_table']['product_weight']
        product_weight_mean = np.round(product_weight_s.median(), 2)
        full_product_weight_s = \
            product_weight_s.reindex(data['index'], level='Producto_ID',
                                     copy=False, fill_value=product_weight_mean)
        full_product_weight_s.reset_index(level=index_to_be_removed, drop=True,
                                          inplace=True)
        assert full_product_weight_s.isnull().sum() == 0, "product weight has NaN"
        return {'product_weight': full_product_weight_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'producto_table'])
    @will_generate('product_volume')
    def gen_product_volume(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        product_volume_s = data['producto_table']['product_volume']

        full_product_volume_s = \
            product_volume_s.reindex(data['index'], level='Producto_ID',
                                     copy=False, fill_value=0)
        full_product_volume_s.reset_index(level=index_to_be_removed, drop=True,
                                          inplace=True)
        assert full_product_volume_s.isnull().sum() == 0, "product volume has NaN"
        return {'product_volume': full_product_volume_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'producto_table'])
    @will_generate('product_pieces')
    def gen_product_pieces(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        product_pieces_s = data['producto_table']['product_pieces']
        full_product_pieces_s = \
            product_pieces_s.reindex(data['index'], level='Producto_ID',
                                     copy=False, fill_value=1)
        full_product_pieces_s = \
            full_product_pieces_s.astype(np.int32, copy=False)
        full_product_pieces_s.reset_index(level=index_to_be_removed, drop=True,
                                          inplace=True)
        assert full_product_pieces_s.isnull().sum() == 0, "product pieces has NaN"
        return {'product_pieces': full_product_pieces_s}

    ###########################################
    # Generate features: sales depot features #
    ###########################################
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'agencia_table'])
    @will_generate(['sales_depot_town', 'sales_depot_state'])
    def gen_sales_depot_subinfo(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        depot_subinfo_rename_dict = \
            {'Town': 'sales_depot_town', 'State': 'sales_depot_state'}
        depot_features = []
        for old_cat, new_cat in depot_subinfo_rename_dict.items():
            sales_depot_subinfo_s = data['agencia_table'][old_cat]
            sales_depot_subinfo_s = _object_encode(sales_depot_subinfo_s)
            sales_depot_subinfo_s.rename(new_cat, inplace=True)
            depot_features.append(sales_depot_subinfo_s)
            full_df = pd.concat(depot_features, axis=1)
            full_df = full_df.reindex(data['index'], level='Agencia_ID',
                                      fill_value=0, copy=False)
            full_df = full_df.astype(np.int32, copy=True)
            full_df.reset_index(level=index_to_be_removed,
                                drop=True, inplace=True)
        return full_df
