from ..decorators import will_generate
from ..registered_decorators import (require_intermediate_data,
                                     intermediate_data)


class IntermediateDataGeneratorMixin(object):
    @intermediate_data(skip_if_exist=True, show_skip=False)
    @will_generate('train_transaction')
    def gen_train_transaction(self):
        return {'train_transaction': self.data_store['train_transaction']}

    @intermediate_data(skip_if_exist=True, show_skip=False)
    @will_generate('test_transaction')
    def gen_test_transaction(self):
        return {'test_transaction': self.data_store['test_transaction']}

    @intermediate_data(skip_if_exist=True, show_skip=False)
    @will_generate('cliente_table')
    def gen_cliente_table(self):
        return {'cliente_table': self.data_store['cliente_table']}

    @intermediate_data(skip_if_exist=True, show_skip=False)
    @will_generate('producto_table')
    def gen_producto_table(self):
        return {'producto_table': self.data_store['producto_table']}

    @intermediate_data(skip_if_exist=True, show_skip=False)
    @will_generate('agencia_table')
    def gen_agencia_table(self):
        return {'agencia_table': self.data_store['agencia_table']}

    @intermediate_data(skip_if_exist=True, show_skip=False)
    @will_generate('transaction')
    def gen_transaction(self):
        return {'transaction': self.data_store['transaction']}

    @intermediate_data(skip_if_exist=True, show_skip=False)
    @require_intermediate_data(['transaction'])
    @will_generate('index')
    def gen_index(self, data):
        index = data['transaction'].index
        # force pandas to precompute the indexing things
        # pd.Series().reindex(index=index)
        return {'index': index}

