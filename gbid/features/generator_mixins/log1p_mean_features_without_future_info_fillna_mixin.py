import numpy as np

from .feature_without_future_info_mixin import (features_without_future_info_groupby_fillna_fix,
                                                features_without_future_info_groupby_fillna_mean_fix)
from ..decorators import will_generate
from ..registered_decorators import require_intermediate_data, features

class Log1pMeanFeaturesWithoutFutureInfoFillnaMixin(object):

    # group by 1 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_fillna_m1_fix',
            },
            fill_value=-1,            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_depot_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_depot_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_depot_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_route_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_route_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Ruta_SAK',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_route_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    # group by 2 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_product_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_product_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Producto_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_product_pair_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_channel_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_channel_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_channel_pair_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_depot_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_depot_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_depot_pair_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_route_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_route_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_route_pair_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_channel_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_channel_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_channel_pair_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_depot_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_depot_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_depot_pair_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_route_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_route_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_route_pair_fillna_m1_fix',
            },
            fill_value=-1,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_depot_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_depot_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_depot_pair_fillna_m1_fix',
            },
            fill_value=-1, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_route_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_route_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_route_pair_fillna_m1_fix',
            },
            fill_value=-1, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_depot_route_pair_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_depot_route_pair_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_depot_route_pair_fillna_m1_fix',
            },
            fill_value=-1, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)

        return feature_df

    # group by 3 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_depot_triplet_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_depot_triplet_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_depot_triplet_fillna_m1_fix',
            },
            fill_value=-1, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_route_triplet_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_route_triplet_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_route_triplet_fillna_m1_fix',
            },
            fill_value=-1, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_depot_route_triplet_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_depot_route_triplet_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_depot_route_triplet_fillna_m1_fix',
            },
            fill_value=-1, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_channel_depot_quadruplet_fillna_m1_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_channel_depot_quadruplet_fillna_m1_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_channel_depot_quadruplet_fillna_m1_fix',
            },
            fill_value=-1, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        
        return feature_df


###########################################################################################################
###########################################################################################################

    # group by 1 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_fillna_0_fix',
            },
            fill_value=0,            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_depot_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_depot_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_depot_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_route_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_route_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Ruta_SAK',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_route_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    # group by 2 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_product_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_product_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Producto_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_product_pair_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_channel_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_channel_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_channel_pair_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_depot_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_depot_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_depot_pair_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_route_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_route_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_route_pair_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_channel_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_channel_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_channel_pair_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_depot_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_depot_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_depot_pair_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_route_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_route_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_route_pair_fillna_0_fix',
            },
            fill_value=0,
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_depot_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_depot_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_depot_pair_fillna_0_fix',
            },
            fill_value=0, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_route_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_route_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_route_pair_fillna_0_fix',
            },
            fill_value=0, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_depot_route_pair_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_depot_route_pair_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_depot_route_pair_fillna_0_fix',
            },
            fill_value=0, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)

        return feature_df

    # group by 3 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_depot_triplet_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_depot_triplet_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_depot_triplet_fillna_0_fix',
            },
            fill_value=0, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_route_triplet_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_route_triplet_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_route_triplet_fillna_0_fix',
            },
            fill_value=0, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_depot_route_triplet_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_depot_route_triplet_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_depot_route_triplet_fillna_0_fix',
            },
            fill_value=0, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_channel_depot_quadruplet_fillna_0_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_channel_depot_quadruplet_fillna_0_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_channel_depot_quadruplet_fillna_0_fix',
            },
            fill_value=0, 
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        
        return feature_df

############################################################################################################################
############################################################################################################################

    # group by 1 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_fillna_mean_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_fillna_mean_fix',
            },          
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_fillna_mean_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_depot_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_depot_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_depot_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_route_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_route_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Ruta_SAK',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_route_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    # group by 2 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_product_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_product_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Producto_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_product_pair_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_channel_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_channel_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_channel_pair_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_depot_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_depot_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_depot_pair_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_route_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_route_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_route_pair_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_channel_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_channel_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_channel_pair_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_depot_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_depot_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_depot_pair_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_route_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_route_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_route_pair_fillna_mean_fix',
            },            
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_depot_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_depot_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_depot_pair_fillna_mean_fix',
            },             
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_route_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_route_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_route_pair_fillna_mean_fix',
            },             
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_depot_route_pair_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_depot_route_pair_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_depot_route_pair_fillna_mean_fix',
            },             
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)

        return feature_df

    # group by 3 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_depot_triplet_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_depot_triplet_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_depot_triplet_fillna_mean_fix',
            },             
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_route_triplet_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_route_triplet_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_route_triplet_fillna_mean_fix',
            },             
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_depot_route_triplet_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_depot_route_triplet_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_depot_route_triplet_fillna_mean_fix',
            },             
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_channel_depot_quadruplet_fillna_mean_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_channel_depot_quadruplet_fillna_mean_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fillna_mean_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_channel_depot_quadruplet_fillna_mean_fix',
            },             
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        
        return feature_df
