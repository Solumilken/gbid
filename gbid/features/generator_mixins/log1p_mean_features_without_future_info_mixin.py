import numpy as np

from .feature_without_future_info_mixin import features_without_future_info_groupby_fix
from ..decorators import will_generate
from ..registered_decorators import require_intermediate_data, features


class Log1pMeanFeaturesWithoutFutureInfoMixin(object):

    # group by 1 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_depot_fix')
    def gen_log1p_mean_features_without_future_info_by_same_depot_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_depot_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_route_fix')
    def gen_log1p_mean_features_without_future_info_by_same_route_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Ruta_SAK',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_route_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    # group by 2 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_product_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_product_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Producto_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_product_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_channel_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_channel_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_channel_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_depot_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_depot_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_depot_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_client_route_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_client_route_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_client_route_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_channel_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_channel_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_channel_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_depot_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_depot_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_depot_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_product_route_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_route_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_product_route_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_depot_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_depot_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_depot_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_channel_route_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_channel_route_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_channel_route_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_by_same_depot_route_pair_fix')
    def gen_log1p_mean_features_without_future_info_by_same_depot_route_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_by_same_depot_route_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    # group by 3 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_depot_triplet_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_depot_triplet_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_depot_triplet_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_route_triplet_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_route_triplet_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_route_triplet_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_depot_route_triplet_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_depot_route_triplet_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_depot_route_triplet_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_demand_without_future_info_'
                   'by_same_product_client_channel_depot_quadruplet_fix')
    def gen_log1p_mean_features_without_future_info_by_same_product_client_channel_depot_quadruplet_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_demand_without_future_info_'
                    'by_same_product_client_channel_depot_quadruplet_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=lambda x: x)
        return feature_df
