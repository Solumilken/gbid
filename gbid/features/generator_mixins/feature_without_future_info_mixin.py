import numpy as np
import pandas as pd

from ..decorators import will_generate
from ..registered_decorators import require_intermediate_data, features


# def _log1p_mean_expm1_features_without_future_info_single_groupby(target_s, groupby_item,
#                                                                   total_week_numbers,
#                                                                   standard_index,
#                                                                   index_to_be_removed,
#                                                                   feature_name):
#     """deprecated because of same week bug"""
#     log1p_s = np.log1p(target_s)
#     part_feature = []
#     for week in total_week_numbers:
#         week_index = standard_index[standard_index.get_level_values('Semana') == week]
#         history_log1p_s = \
#             log1p_s[log1p_s.index.get_level_values('Semana') <= week]
#         feature_s = \
#             history_log1p_s.groupby(level=groupby_item).mean()
#         feature_s = np.expm1(feature_s)
#         feature_s = feature_s.reindex(week_index, level=groupby_item, copy=False,
#                                       fill_value=feature_s.median())
#         feature_s.reset_index(level=index_to_be_removed, drop=True, inplace=True)
#         part_feature.append(feature_s)

#     feature_s = pd.concat(part_feature)
#     feature_s.rename(feature_name, inplace=True)
#     assert feature_s.isnull().sum() == 0, "there is NaN in this series"
#     assert len(set(feature_s.index)) == len(feature_s), "id is not unique in this series"
#     return feature_s


# def _log1p_mean_expm1_features_without_future_info_multi_groupby(target_s, groupby_items,
#                                                                  total_week_numbers,
#                                                                  standard_df,
#                                                                  index_to_be_removed,  # pylint: disable=W0613
#                                                                  feature_name):
#     """deprecated because of same week bug"""
#     log1p_s = np.log1p(target_s)
#     part_feature = []
#     for week in total_week_numbers:
#         week_df = standard_df[standard_df['Semana'] == week]
#         history_log1p_s = \
#             log1p_s[log1p_s.index.get_level_values('Semana') <= week]
#         feature_s = \
#             history_log1p_s.groupby(level=groupby_items).mean()
#         feature_s = np.expm1(feature_s)
#         feature_df = feature_s.reset_index()
#         week_df = week_df.merge(feature_df, on=groupby_items, how='left')
#         week_df.set_index('id', inplace=True)
#         output_feature_s = week_df[feature_s.name]
#         output_feature_s.fillna(feature_s.median(), inplace=True)
#         part_feature.append(output_feature_s)

#     full_s = pd.concat(part_feature)
#     full_s.rename(feature_name, inplace=True)
#     assert full_s.isnull().sum() == 0, "there is NaN in this series"
#     assert len(set(full_s.index)) == len(full_s), "id is not unique in this series"
#     return full_s


def features_without_future_info_groupby_fix(
        train_transaction_df, groupby, week_numbers, index, features_mapping,
        preprocess_func, process_func, postprocess_func):
    feature_columns = list(features_mapping.keys())
    train_transaction_df = train_transaction_df.loc[:, feature_columns]
    preprocessed_df = preprocess_func(train_transaction_df)
    preprocessed_df.reset_index(inplace=True)
    preprocessed_df = (preprocessed_df
                       .loc[:, ['Semana'] + list(groupby) + feature_columns])

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id', 'Semana'] + list(groupby)]

    feature_dfs = []
    for week in week_numbers:
        week_df = index_df[index_df['Semana'] == week]
        history_df = preprocessed_df[preprocessed_df['Semana'] < week]
        feature_df = process_func(history_df.groupby(groupby)[feature_columns])
        feature_df = postprocess_func(feature_df)
        feature_df.reset_index(inplace=True)
        week_df = week_df.merge(feature_df, on=groupby, how='left')
        week_df.set_index('id', inplace=True)
        week_df = week_df[feature_columns]
        if history_df.shape[0] == 0:
            fill_value = 0
        else:
            fill_value = feature_df[feature_columns].median()
        week_df.fillna(fill_value, inplace=True)
        feature_dfs.append(week_df)

    full_df = pd.concat(feature_dfs)
    full_df.rename(columns=features_mapping, inplace=True)
    assert not full_df.isnull().any().any(), "there is NaN in this DataFrame"
    assert len(set(full_df.index)) == len(full_df), "id is not unique in this DataFrame"

    return full_df

def features_without_future_info_groupby_fillna_mean_fix(
        train_transaction_df, groupby, week_numbers, index, features_mapping,
        preprocess_func, process_func, postprocess_func):
    feature_columns = list(features_mapping.keys())
    train_transaction_df = train_transaction_df.loc[:, feature_columns]
    preprocessed_df = preprocess_func(train_transaction_df)
    preprocessed_df.reset_index(inplace=True)
    preprocessed_df = (preprocessed_df
                       .loc[:, ['Semana'] + list(groupby) + feature_columns])

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id', 'Semana'] + list(groupby)]

    feature_dfs = []
    for week in week_numbers:
        week_df = index_df[index_df['Semana'] == week]
        history_df = preprocessed_df[preprocessed_df['Semana'] < week]
        feature_df = process_func(history_df.groupby(groupby)[feature_columns])
        feature_df = postprocess_func(feature_df)
        feature_df.reset_index(inplace=True)
        week_df = week_df.merge(feature_df, on=groupby, how='left')
        week_df.set_index('id', inplace=True)
        week_df = week_df[feature_columns]
        if history_df.shape[0] == 0:
            fill_value = 0
        else:
            fill_value = feature_df[feature_columns].mean()
        week_df.fillna(fill_value, inplace=True)
        feature_dfs.append(week_df)

    full_df = pd.concat(feature_dfs)
    full_df.rename(columns=features_mapping, inplace=True)
    assert not full_df.isnull().any().any(), "there is NaN in this DataFrame"
    assert len(set(full_df.index)) == len(full_df), "id is not unique in this DataFrame"

    return full_df


def features_without_future_info_groupby_fillna_fix(
        train_transaction_df, groupby, week_numbers, index, 
        features_mapping, fill_value,
        preprocess_func, process_func, postprocess_func):
    feature_columns = list(features_mapping.keys())
    train_transaction_df = train_transaction_df.loc[:, feature_columns]
    preprocessed_df = preprocess_func(train_transaction_df)
    preprocessed_df.reset_index(inplace=True)
    preprocessed_df = (preprocessed_df
                       .loc[:, ['Semana'] + list(groupby) + feature_columns])

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id', 'Semana'] + list(groupby)]

    feature_dfs = []
    for week in week_numbers:
        week_df = index_df[index_df['Semana'] == week]
        history_df = preprocessed_df[preprocessed_df['Semana'] < week]
        feature_df = process_func(history_df.groupby(groupby)[feature_columns])
        feature_df = postprocess_func(feature_df)
        feature_df.reset_index(inplace=True)
        week_df = week_df.merge(feature_df, on=groupby, how='left')
        week_df.set_index('id', inplace=True)
        week_df = week_df[feature_columns]

        week_df.fillna(fill_value, inplace=True)
        feature_dfs.append(week_df)

    full_df = pd.concat(feature_dfs)
    full_df.rename(columns=features_mapping, inplace=True)
    # assert not full_df.isnull().any().any(), "there is NaN in this DataFrame"
    assert len(set(full_df.index)) == len(full_df), "id is not unique in this DataFrame"
    assert not np.isinf(full_df).any().any(), "there is Inf in this DataFrame"
    return full_df

class FeaturesWithoutFutureInformationMixin(object):

    # deprecated because of same week bug
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate(['log1p_mean_expm1_demand_without_future_info_by_same_%s' % cat
                    for cat in ['client', 'product', 'channel', 'depot', 'route']])
    def gen_log1p_mean_expm1_demand_without_future_info_by_same_cat(self, data):
        """deprecated because of same week bug"""
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        factor_variables_rename_dict = {
            'Cliente_ID': 'client', 'Producto_ID': 'product',
            'Canal_ID': 'channel', 'Agencia_ID': 'depot',
            'Ruta_SAK': 'route'}
        feature_dict = {}
        for old_cat, new_cat in factor_variables_rename_dict.items():
            feature_name = \
                'log1p_mean_expm1_demand_without_future_info_by_same_' + new_cat
            feature_s = \
                _log1p_mean_expm1_features_without_future_info_single_groupby(
                    target_s=train_adjusted_demand_s,
                    groupby_item=old_cat,
                    total_week_numbers=range(3, 12),
                    standard_index=data['index'],
                    index_to_be_removed=index_to_be_removed,
                    feature_name=feature_name)
            feature_dict[feature_name] = feature_s
        return feature_dict

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_client_product_pair')
    def gen_log1p_mean_expm1_demand_without_future_info_by_same_client_product_pair(self, data):
        """deprecated because of same week bug"""
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']

        feature_s = _log1p_mean_expm1_features_without_future_info_multi_groupby(
            target_s=train_adjusted_demand_s,
            groupby_items=['Cliente_ID', 'Producto_ID'],
            total_week_numbers=range(3, 12),
            standard_df=pd.DataFrame(index=data['index']).reset_index(),
            index_to_be_removed=index_to_be_removed,
            feature_name='log1p_mean_expm1_demand_without_future_info_'
                         'by_same_client_product_pair')
        return {'log1p_mean_expm1_demand_without_future_info_'
                'by_same_client_product_pair': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_product_depot_pair')
    def gen_log1p_mean_expm1_demand_without_future_info_by_same_product_depot_pair(self, data):
        """deprecated because of same week bug"""
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']

        feature_s = _log1p_mean_expm1_features_without_future_info_multi_groupby(
            target_s=train_adjusted_demand_s,
            groupby_items=['Producto_ID', 'Agencia_ID'],
            total_week_numbers=range(3, 12),
            standard_df=pd.DataFrame(index=data['index']).reset_index(),
            index_to_be_removed=index_to_be_removed,
            feature_name='log1p_mean_expm1_demand_without_future_info_'
                         'by_same_product_depot_pair')
        return {'log1p_mean_expm1_demand_without_future_info_'
                'by_same_product_depot_pair': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_product_route_pair')
    def gen_log1p_mean_expm1_demand_without_future_info_by_same_product_route_pair(self, data):
        """deprecated because of same week bug"""
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']

        feature_s = _log1p_mean_expm1_features_without_future_info_multi_groupby(
            target_s=train_adjusted_demand_s,
            groupby_items=['Producto_ID', 'Ruta_SAK'],
            total_week_numbers=range(3, 12),
            standard_df=pd.DataFrame(index=data['index']).reset_index(),
            index_to_be_removed=index_to_be_removed,
            feature_name='log1p_mean_expm1_demand_without_future_info_'
                         'by_same_product_route_pair')
        return {'log1p_mean_expm1_demand_without_future_info_by_same_product_route_pair': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_'
                   'by_same_product_client_depot_triplet')
    def gen_log1p_mean_expm1_demand_without_future_info_by_same_product_client_depot_triplet(
            self, data):
        """deprecated because of same week bug"""
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']

        feature_s = _log1p_mean_expm1_features_without_future_info_multi_groupby(
            target_s=train_adjusted_demand_s,
            groupby_items=['Producto_ID', 'Cliente_ID', 'Agencia_ID'],
            total_week_numbers=range(3, 12),
            standard_df=pd.DataFrame(index=data['index']).reset_index(),
            index_to_be_removed=index_to_be_removed,
            feature_name='log1p_mean_expm1_demand_without_future_info_'
                         'by_same_product_client_depot_triplet')
        return {'log1p_mean_expm1_demand_without_future_info_'
                'by_same_product_client_depot_triplet': feature_s}

    # group by 1 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_client_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_client_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_client_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_product_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_product_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_product_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_channel_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_channel_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_channel_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_depot_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_depot_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_depot_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_route_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_route_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Ruta_SAK',),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_route_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    # group by 2 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_client_product_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_client_product_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Producto_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_client_product_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_client_channel_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_client_channel_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_client_channel_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_client_depot_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_client_depot_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_client_depot_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_client_route_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_client_route_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Cliente_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_client_route_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_product_channel_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_product_channel_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Canal_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_product_channel_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_product_depot_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_product_depot_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_product_depot_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_product_route_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_product_route_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_product_route_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_channel_depot_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_channel_depot_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_channel_depot_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_channel_route_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_channel_route_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Canal_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_channel_route_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_by_same_depot_route_pair_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_depot_route_pair_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Agencia_ID', 'Ruta_SAK'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_by_same_depot_route_pair_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df

    # group by 3 id
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_mean_expm1_demand_without_future_info_'
                   'by_same_product_client_depot_triplet_fix')
    def gen_log1p_mean_expm1_features_without_future_info_by_same_product_client_depot_triplet_fix(
            self, data):
        feature_df = features_without_future_info_groupby_fix(
            train_transaction_df=data['train_transaction'],
            groupby=('Producto_ID', 'Cliente_ID', 'Agencia_ID'),
            week_numbers=range(3, 12),
            index=data['index'],
            features_mapping={
                'Demanda_uni_equil':
                    'log1p_mean_expm1_demand_without_future_info_'
                    'by_same_product_client_depot_triplet_fix',
            },
            preprocess_func=np.log1p,
            process_func=lambda groupby: groupby.mean(),
            postprocess_func=np.expm1)
        return feature_df
