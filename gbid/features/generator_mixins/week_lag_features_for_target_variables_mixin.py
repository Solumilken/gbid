import numpy as np
import pandas as pd

from ..decorators import will_generate
from ..registered_decorators import (require_intermediate_data, features)


def _week_lags_features_groupby_fillna(
        train_transaction_df, index, groupby, 
        features_mapping, number_week_lags, 
        preprocess_func, process_func, postprocess_func, fill_value=0):
    feature_columns = list(features_mapping.keys())
    train_transaction_df = train_transaction_df.loc[:, feature_columns]

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id'] + list(groupby)]

    preprocessed_df = preprocess_func(train_transaction_df)
    preprocessed_df.reset_index(inplace=True)
    preprocessed_df = (preprocessed_df
                       .loc[:, list(groupby) + feature_columns])
    preprocessed_df['Semana'] = preprocessed_df['Semana'] + number_week_lags

    feature_df = \
        process_func(preprocessed_df.groupby(groupby)[feature_columns])
    feature_df = postprocess_func(feature_df)
    feature_df.reset_index(inplace=True)

    full_df = index_df.merge(feature_df, on=groupby, how='left')
    full_df.fillna(fill_value, inplace=True)

    full_df.set_index('id', inplace=True)
    full_df = full_df[feature_columns]
    full_df.rename(columns=features_mapping, inplace=True)

    assert not full_df.isnull().any().any(), "there is NaN in this DataFrame"
    assert not np.isinf(full_df).any().any(), "there is Inf in this DataFrame"
    return full_df

def _week_lags_features_groupby_fillna_mean(
        train_transaction_df, index, groupby, 
        features_mapping, number_week_lags, 
        preprocess_func, process_func, postprocess_func):
    feature_columns = list(features_mapping.keys())
    train_transaction_df = train_transaction_df.loc[:, feature_columns]

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id'] + list(groupby)]

    preprocessed_df = preprocess_func(train_transaction_df)
    preprocessed_df.reset_index(inplace=True)
    preprocessed_df = (preprocessed_df
                       .loc[:, list(groupby) + feature_columns])
    preprocessed_df['Semana'] = preprocessed_df['Semana'] + number_week_lags

    feature_df = \
        process_func(preprocessed_df.groupby(groupby)[feature_columns])
    feature_df = postprocess_func(feature_df)
    feature_df.reset_index(inplace=True)

    full_df = index_df.merge(feature_df, on=groupby, how='left')
    full_df.fillna(feature_df[feature_columns].mean(), inplace=True)

    full_df.set_index('id', inplace=True)
    full_df = full_df[feature_columns]
    full_df.rename(columns=features_mapping, inplace=True)

    assert not full_df.isnull().any().any(), "there is NaN in this DataFrame"
    assert not np.isinf(full_df).any().any(), "there is Inf in this DataFrame"
    return full_df

def _week_lags_features_groupby_fillna_median(
        train_transaction_df, index, groupby, 
        features_mapping, number_week_lags, 
        preprocess_func, process_func, postprocess_func):
    feature_columns = list(features_mapping.keys())
    train_transaction_df = train_transaction_df.loc[:, feature_columns]

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id'] + list(groupby)]

    preprocessed_df = preprocess_func(train_transaction_df)
    preprocessed_df.reset_index(inplace=True)
    preprocessed_df = (preprocessed_df
                       .loc[:, list(groupby) + feature_columns])
    preprocessed_df['Semana'] = preprocessed_df['Semana'] + number_week_lags

    feature_df = \
        process_func(preprocessed_df.groupby(groupby)[feature_columns])
    feature_df = postprocess_func(feature_df)
    feature_df.reset_index(inplace=True)

    full_df = index_df.merge(feature_df, on=groupby, how='left')
    full_df.fillna(feature_df[feature_columns].median(), inplace=True)

    full_df.set_index('id', inplace=True)
    full_df = full_df[feature_columns]
    full_df.rename(columns=features_mapping, inplace=True)

    assert not full_df.isnull().any().any(), "there is NaN in this DataFrame"
    assert not np.isinf(full_df).any().any(), "there is Inf in this DataFrame"
    return full_df

#############################################################################################
def _week_lags_feature_frequency_groupby_fillna(
        transaction_df, index, groupby, 
        feature_name, number_week_lags, fill_value=0):
    transaction_df['Semana'] = transaction_df['Semana'] + number_week_lags
    feature_s = transaction_df.groupby(groupby).size()
    feature_s.rename(feature_name, inplace=True) 
    feature_df = feature_s.reset_index()
    feature_df = feature_df[feature_df['Semana'] < 12]

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id'] + list(groupby)]

    full_df = index_df.merge(feature_df, on=groupby, how='left')
    full_df.fillna(fill_value, inplace=True)

    full_df.set_index('id', inplace=True)
    full_s = full_df[feature_name]

    assert full_s.isnull().sum() == 0, "there is NaN in this series"
    assert np.isinf(full_s).sum() == 0, "there is Inf in this series"
    return full_s

def _week_lags_feature_frequency_groupby_fillna_mean(
        transaction_df, index, groupby, 
        feature_name, number_week_lags):
    transaction_df['Semana'] = transaction_df['Semana'] + number_week_lags
    feature_s = transaction_df.groupby(groupby).size()
    feature_s.rename(feature_name, inplace=True) 
    feature_df = feature_s.reset_index()
    feature_df = feature_df[feature_df['Semana'] < 12]

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id'] + list(groupby)]

    full_df = index_df.merge(feature_df, on=groupby, how='left')
    full_df.set_index('id', inplace=True)
    
    full_s = full_df[feature_name]
    full_s.fillna(full_s.mean(), inplace=True)

    assert full_s.isnull().sum() == 0, "there is NaN in this series"
    assert np.isinf(full_s).sum() == 0, "there is Inf in this series"
    return full_s

def _week_lags_feature_frequency_groupby_fillna_median(
        transaction_df, index, groupby, 
        feature_name, number_week_lags):
    transaction_df['Semana'] = transaction_df['Semana'] + number_week_lags
    feature_s = transaction_df.groupby(groupby).size()
    feature_s.rename(feature_name, inplace=True) 
    feature_df = feature_s.reset_index()
    feature_df = feature_df[feature_df['Semana'] < 12]

    index_df = pd.DataFrame(index=index)
    index_df.reset_index(inplace=True)
    index_df = index_df.loc[:, ['id'] + list(groupby)]

    full_df = index_df.merge(feature_df, on=groupby, how='left')
    full_df.set_index('id', inplace=True)
    
    full_s = full_df[feature_name]
    full_s.fillna(full_s.median(), inplace=True)

    assert full_s.isnull().sum() == 0, "there is NaN in this series"
    assert np.isinf(full_s).sum() == 0, "there is Inf in this series"
    return full_s


#######################################################################################################################

class WeekLagFeaturesMixin(object):

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('1weeklag_log1p_mean_demand_by_week_client_product_triplet_fillna_0')
    def gen_1weeklag_log1p_mean_features_by_week_client_product_triplet_fillna_0(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil':
                    '1weeklag_log1p_mean_demand_by_week_client_product_triplet_fillna_0',
                },
                number_week_lags=1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('2weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_0')
    def gen_2weeklags_log1p_mean_features_by_week_client_product_triplet_fillna_0(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '2weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_0',
                },
                number_week_lags=2,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('3weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_0')
    def gen_3weeklags_log1p_mean_features_by_week_client_product_triplet_fillna_0(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '3weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_0',
                },
                number_week_lags=3,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

# week product triplet

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('1weeklag_log1p_mean_demand_by_week_product_pair_fillna_0')
    def gen_1weeklag_log1p_mean_features_by_week_product_pair_fillna_0(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil':
                    '1weeklag_log1p_mean_demand_by_week_product_pair_fillna_0',
                },
                number_week_lags=1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('2weeklags_log1p_mean_demand_by_week_product_pair_fillna_0')
    def gen_2weeklags_log1p_mean_features_by_week_product_pair_fillna_0(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '2weeklags_log1p_mean_demand_by_week_product_pair_fillna_0',
                },
                number_week_lags=2,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('3weeklags_log1p_mean_demand_by_week_product_pair_fillna_0')
    def gen_3weeklags_log1p_mean_features_by_week_product_pair_fillna_0(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '3weeklags_log1p_mean_demand_by_week_product_pair_fillna_0',
                },
                number_week_lags=3,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


# week frequency

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('0weeklag_frequency_by_week_product_pair_fillna_0')
    def gen_0weeklag_frequency_by_week_product_pair_fillna_0(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='0weeklag_frequency_by_week_product_pair_fillna_0',
                number_week_lags=0)
        return {'0weeklag_frequency_by_week_product_pair_fillna_0': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('1weeklag_frequency_by_week_product_pair_fillna_0')
    def gen_1weeklag_frequency_by_week_product_pair_fillna_0(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='1weeklag_frequency_by_week_product_pair_fillna_0',
                number_week_lags=1)
        return {'1weeklag_frequency_by_week_product_pair_fillna_0': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('2weeklags_frequency_by_week_product_pair_fillna_0')
    def gen_2weeklags_frequency_by_week_product_pair_fillna_0(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='2weeklags_frequency_by_week_product_pair_fillna_0',
                number_week_lags=2)
        return {'2weeklags_frequency_by_week_product_pair_fillna_0': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('3weeklags_frequency_by_week_product_pair_fillna_0')
    def gen_3weeklags_frequency_by_week_product_pair_fillna_0(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='3weeklags_frequency_by_week_product_pair_fillna_0',
                number_week_lags=3)
        return {'3weeklags_frequency_by_week_product_pair_fillna_0': feature_s}

#################################################################################################
#################################################################################################

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('1weeklag_log1p_mean_demand_by_week_client_product_triplet_fillna_m1')
    def gen_1weeklag_log1p_mean_features_by_week_client_product_triplet_fillna_m1(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil':
                    '1weeklag_log1p_mean_demand_by_week_client_product_triplet_fillna_m1',
                },
                number_week_lags=1,
                fill_value=-1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('2weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_m1')
    def gen_2weeklags_log1p_mean_features_by_week_client_product_triplet_fillna_m1(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '2weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_m1',
                },
                number_week_lags=2,
                fill_value=-1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('3weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_m1')
    def gen_3weeklags_log1p_mean_features_by_week_client_product_triplet_fillna_m1(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '3weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_m1',
                },
                number_week_lags=3,
                fill_value=-1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

# more week_lag (week + product)

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('1weeklag_log1p_mean_demand_by_week_product_pair_fillna_m1')
    def gen_1weeklag_log1p_mean_demand_by_week_product_pair_fillna_m1(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil':
                    '1weeklag_log1p_mean_demand_by_week_product_pair_fillna_m1',
                },
                number_week_lags=1,
                fill_value=-1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('2weeklags_log1p_mean_demand_by_week_product_pair_fillna_m1')
    def gen_2weeklags_log1p_mean_demand_by_week_product_pair_fillna_m1(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '2weeklags_log1p_mean_demand_by_week_product_pair_fillna_m1',
                },
                number_week_lags=2,
                fill_value=-1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('3weeklags_log1p_mean_demand_by_week_product_pair_fillna_m1')
    def gen_3weeklags_log1p_mean_demand_by_week_product_pair_fillna_m1(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '3weeklags_log1p_mean_demand_by_week_product_pair_fillna_m1',
                },
                number_week_lags=3,
                fill_value=-1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

# week frequency

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('0weeklag_frequency_by_week_product_pair_fillna_m1')
    def gen_0weeklag_frequency_by_week_product_pair_fillna_m1(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='0weeklag_frequency_by_week_product_pair_fillna_m1',
                fill_value=-1,
                number_week_lags=0)
        return {'0weeklag_frequency_by_week_product_pair_fillna_m1': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('1weeklag_frequency_by_week_product_pair_fillna_m1')
    def gen_1weeklag_frequency_by_week_product_pair_fillna_m1(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='1weeklag_frequency_by_week_product_pair_fillna_m1',
                fill_value=-1,
                number_week_lags=1)
        return {'1weeklag_frequency_by_week_product_pair_fillna_m1': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('2weeklags_frequency_by_week_product_pair_fillna_m1')
    def gen_2weeklags_frequency_by_week_product_pair_fillna_m1(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='2weeklags_frequency_by_week_product_pair_fillna_m1', 
                fill_value=-1,
                number_week_lags=2)
        return {'2weeklags_frequency_by_week_product_pair_fillna_m1': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('3weeklags_frequency_by_week_product_pair_fillna_m1')
    def gen_3weeklags_frequency_by_week_product_pair_fillna_m1(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='3weeklags_frequency_by_week_product_pair_fillna_m1',
                fill_value=-1,
                number_week_lags=3)
        return {'3weeklags_frequency_by_week_product_pair_fillna_m1': feature_s}



#####################################################################################################################
#####################################################################################################################

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('1weeklag_log1p_mean_demand_by_week_client_product_triplet_fillna_mean')
    def gen_1weeklag_log1p_mean_features_by_week_client_product_triplet_fillna_mean(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_mean(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil':
                    '1weeklag_log1p_mean_demand_by_week_client_product_triplet_fillna_mean',
                },
                number_week_lags=1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('2weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_mean')
    def gen_2weeklags_log1p_mean_features_by_week_client_product_triplet_fillna_mean(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_mean(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '2weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_mean',
                },
                number_week_lags=2,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('3weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_mean')
    def gen_3weeklags_log1p_mean_features_by_week_client_product_triplet_fillna_mean(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_mean(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '3weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_mean',
                },
                number_week_lags=3,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

# week product triplet

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('1weeklag_log1p_mean_demand_by_week_product_pair_fillna_mean')
    def gen_1weeklag_log1p_mean_features_by_week_product_pair_fillna_mean(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_mean(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil':
                    '1weeklag_log1p_mean_demand_by_week_product_pair_fillna_mean',
                },
                number_week_lags=1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('2weeklags_log1p_mean_demand_by_week_product_pair_fillna_mean')
    def gen_2weeklags_log1p_mean_features_by_week_product_pair_fillna_mean(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_mean(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '2weeklags_log1p_mean_demand_by_week_product_pair_fillna_mean',
                },
                number_week_lags=2,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('3weeklags_log1p_mean_demand_by_week_product_pair_fillna_mean')
    def gen_3weeklags_log1p_mean_features_by_week_product_pair_fillna_mean(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_mean(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '3weeklags_log1p_mean_demand_by_week_product_pair_fillna_mean',
                },
                number_week_lags=3,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


# week frequency
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('0weeklag_frequency_by_week_product_pair_fillna_mean')
    def gen_0weeklag_frequency_by_week_product_pair_fillna_mean(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna_mean(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='0weeklag_frequency_by_week_product_pair_fillna_mean',
                number_week_lags=0)
        return {'0weeklag_frequency_by_week_product_pair_fillna_mean': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('1weeklag_frequency_by_week_product_pair_fillna_mean')
    def gen_1weeklag_frequency_by_week_product_pair_fillna_mean(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna_mean(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='1weeklag_frequency_by_week_product_pair_fillna_mean',
                number_week_lags=1)
        return {'1weeklag_frequency_by_week_product_pair_fillna_mean': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('2weeklags_frequency_by_week_product_pair_fillna_mean')
    def gen_2weeklags_frequency_by_week_product_pair_fillna_mean(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna_mean(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='2weeklags_frequency_by_week_product_pair_fillna_mean',
                number_week_lags=2)
        return {'2weeklags_frequency_by_week_product_pair_fillna_mean': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('3weeklags_frequency_by_week_product_pair_fillna_mean')
    def gen_3weeklags_frequency_by_week_product_pair_fillna_mean(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna_mean(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='3weeklags_frequency_by_week_product_pair_fillna_mean',
                number_week_lags=3)
        return {'3weeklags_frequency_by_week_product_pair_fillna_mean': feature_s}

######################################################################################################################
######################################################################################################################

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('1weeklag_log1p_mean_demand_by_week_client_product_triplet_fillna_median')
    def gen_1weeklag_log1p_mean_features_by_week_client_product_triplet_fillna_median(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_median(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil':
                    '1weeklag_log1p_mean_demand_by_week_client_product_triplet_fillna_median',
                },
                number_week_lags=1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('2weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_median')
    def gen_2weeklags_log1p_mean_features_by_week_client_product_triplet_fillna_median(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_median(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '2weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_median',
                },
                number_week_lags=2,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('3weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_median')
    def gen_3weeklags_log1p_mean_features_by_week_client_product_triplet_fillna_median(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_median(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Cliente_ID', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '3weeklags_log1p_mean_demand_by_week_client_product_triplet_fillna_median',
                },
                number_week_lags=3,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

# week product triplet

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('1weeklag_log1p_mean_demand_by_week_product_pair_fillna_median')
    def gen_1weeklag_log1p_mean_features_by_week_product_pair_fillna_median(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_median(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil':
                    '1weeklag_log1p_mean_demand_by_week_product_pair_fillna_median',
                },
                number_week_lags=1,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('2weeklags_log1p_mean_demand_by_week_product_pair_fillna_median')
    def gen_2weeklags_log1p_mean_features_by_week_product_pair_fillna_median(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_median(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '2weeklags_log1p_mean_demand_by_week_product_pair_fillna_median',
                },
                number_week_lags=2,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('3weeklags_log1p_mean_demand_by_week_product_pair_fillna_median')
    def gen_3weeklags_log1p_mean_features_by_week_product_pair_fillna_median(self, data):
        feature_df = \
            _week_lags_features_groupby_fillna_median(
                train_transaction_df=data['train_transaction'],
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                features_mapping={
                    'Demanda_uni_equil': 
                    '3weeklags_log1p_mean_demand_by_week_product_pair_fillna_median',
                },
                number_week_lags=3,
                preprocess_func=np.log1p,
                process_func=lambda groupby: groupby.mean(),
                postprocess_func=lambda x: x)
        return feature_df


# week frequency

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('0weeklag_frequency_by_week_product_pair_fillna_median')
    def gen_0weeklag_frequency_by_week_product_pair_fillna_median(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna_median(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='0weeklag_frequency_by_week_product_pair_fillna_median',
                number_week_lags=0)
        return {'0weeklag_frequency_by_week_product_pair_fillna_median': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('1weeklag_frequency_by_week_product_pair_fillna_median')
    def gen_1weeklag_frequency_by_week_product_pair_fillna_median(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna_median(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='1weeklag_frequency_by_week_product_pair_fillna_median',
                number_week_lags=1)
        return {'1weeklag_frequency_by_week_product_pair_fillna_median': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('2weeklags_frequency_by_week_product_pair_fillna_median')
    def gen_2weeklags_frequency_by_week_product_pair_fillna_median(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna_median(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='2weeklags_frequency_by_week_product_pair_fillna_median',
                number_week_lags=2)
        return {'2weeklags_frequency_by_week_product_pair_fillna_median': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction'])
    @will_generate('3weeklags_frequency_by_week_product_pair_fillna_median')
    def gen_3weeklags_frequency_by_week_product_pair_fillna_median(self, data):
        feature_s = \
            _week_lags_feature_frequency_groupby_fillna_median(
                transaction_df=data['transaction'].reset_index(),
                index=data['index'],
                groupby=('Semana', 'Producto_ID'),
                feature_name='3weeklags_frequency_by_week_product_pair_fillna_median',
                number_week_lags=3)
        return {'3weeklags_frequency_by_week_product_pair_fillna_median': feature_s}