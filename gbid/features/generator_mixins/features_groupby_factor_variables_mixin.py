import numpy as np
import pandas as pd

from ..decorators import will_generate
from ..registered_decorators import (require_intermediate_data, features, intermediate_data)

class FeaturesGroupbyFactorVariablesMixin(object):

    #######################################################
    # Generate features: demand + groupby single index    #
    #######################################################
    def _transformed_mean_series_single_index_train(self, series, groupby_target,
                                                    feature_name, standard_index,
                                                    train_dataset_or_not=True):
        index_to_be_removed = list(set(standard_index.names) - set(['id']))
        transformed_series = np.log(series + 1)
        feature_s = transformed_series.groupby(level=groupby_target).mean()
        feature_s = np.exp(feature_s) - 1
        feature_s.rename(feature_name, inplace=True)

        if train_dataset_or_not is False:
            feature_s.reset_index(
                level=groupby_target, drop=True, inplace=True)

        feature_s = feature_s.reindex(
            standard_index, level=groupby_target,
            fill_value=np.median(feature_s), copy=False)
        feature_s.reset_index(
            level=index_to_be_removed, drop=True, inplace=True)
        return feature_s

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_client')
    def gen_demand_by_same_client(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_single_index_train(
                series=adjusted_demand_s,
                groupby_target='Cliente_ID',
                feature_name='adjusted_demand_by_same_client',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_client': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_client_without_last_2_weeks')
    def gen_demand_by_same_client_without_last_2_weeks(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        adjusted_demand_s = \
            adjusted_demand_s[adjusted_demand_s.index.get_level_values('Semana') < 8]
        feature_s = \
            self._transformed_mean_series_single_index_train(
                series=adjusted_demand_s,
                groupby_target='Cliente_ID',
                feature_name='adjusted_demand_by_same_client_without_last_2_weeks',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_client_without_last_2_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_product')
    def gen_demand_by_same_product(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_single_index_train(
                series=adjusted_demand_s,
                groupby_target='Producto_ID',
                feature_name='adjusted_demand_by_same_product',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_product': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_product_without_last_2_weeks')
    def gen_demand_by_same_product_without_last_2_weeks(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        adjusted_demand_s = \
            adjusted_demand_s[adjusted_demand_s.index.get_level_values('Semana') < 8]
        feature_s = \
            self._transformed_mean_series_single_index_train(
                series=adjusted_demand_s,
                groupby_target='Producto_ID',
                feature_name='adjusted_demand_by_same_product_without_last_2_weeks',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_product_without_last_2_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_channel')
    def gen_demand_by_same_channel(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_single_index_train(
                series=adjusted_demand_s,
                groupby_target='Canal_ID',
                feature_name='adjusted_demand_by_same_channel',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_channel': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_depot')
    def gen_demand_by_same_depot(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_single_index_train(
                series=adjusted_demand_s,
                groupby_target='Agencia_ID',
                feature_name='adjusted_demand_by_same_depot',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_depot': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_route')
    def gen_demand_by_same_route(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_single_index_train(
                series=adjusted_demand_s,
                groupby_target='Ruta_SAK',
                feature_name='adjusted_demand_by_same_route',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_route': feature_s}

    def _transformed_mean_series_multi_index_train(self, series,
                                                   groupby_targets,
                                                   feature_name,
                                                   standard_index):
        standard_index_df = pd.DataFrame(
            index=standard_index).reset_index()
        transformed_series = np.log(series + 1)
        feature_s = transformed_series.groupby(
            level=groupby_targets).mean()
        feature_s = np.exp(feature_s) - 1
        feature_s.rename(feature_name, inplace=True)
        standard_index_df = \
            standard_index_df.merge(feature_s.reset_index(),
                                    on=groupby_targets, how='left')
        standard_index_df[feature_name].fillna(np.round(np.median(feature_s), 4),
                                               inplace=True)
        standard_index_df.set_index('id', inplace=True)
        return standard_index_df[feature_name]

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_client_product_pair')
    def gen_demand_by_same_client_product_pair(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_multi_index_train(
                series=adjusted_demand_s,
                groupby_targets=['Cliente_ID', 'Producto_ID'],
                feature_name='adjusted_demand_by_same_client_product_pair',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_client_product_pair': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_product_depot_pair')
    def gen_demand_by_same_product_depot_pair(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_multi_index_train(
                series=adjusted_demand_s,
                groupby_targets=['Producto_ID', 'Agencia_ID'],
                feature_name='adjusted_demand_by_same_product_depot_pair',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_product_depot_pair': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_product_route_pair')
    def gen_demand_by_same_product_route_pair(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_multi_index_train(
                series=adjusted_demand_s,
                groupby_targets=['Producto_ID', 'Ruta_SAK'],
                feature_name='adjusted_demand_by_same_product_route_pair',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_product_route_pair': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_product_client_depot_triplet')
    def gen_demand_by_same_product_client_depot_triplet(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        feature_s = \
            self._transformed_mean_series_multi_index_train(
                series=adjusted_demand_s,
                groupby_targets=['Producto_ID', 'Cliente_ID', 'Agencia_ID'],
                feature_name='adjusted_demand_by_same_product_client_depot_triplet',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_product_client_depot_triplet': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_client_product_pair_without_last_2_weeks')
    def gen_demand_by_same_client_product_pair_without_last_2_weeks(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        adjusted_demand_s = \
            adjusted_demand_s[
                adjusted_demand_s.index.get_level_values('Semana') < 8]
        feature_s = \
            self._transformed_mean_series_multi_index_train(
                series=adjusted_demand_s,
                groupby_targets=['Cliente_ID', 'Producto_ID'],
                feature_name='adjusted_demand_by_same_client_product_pair_without_last_2_weeks',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_client_product_pair_without_last_2_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_product_depot_pair_without_last_2_weeks')
    def gen_demand_by_same_product_depot_pair_without_last_2_weeks(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        adjusted_demand_s = \
            adjusted_demand_s[
                adjusted_demand_s.index.get_level_values('Semana') < 8]
        feature_s = \
            self._transformed_mean_series_multi_index_train(
                series=adjusted_demand_s,
                groupby_targets=['Producto_ID', 'Agencia_ID'],
                feature_name='adjusted_demand_by_same_product_depot_pair_without_last_2_weeks',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_product_depot_pair_without_last_2_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_product_route_pair_without_last_2_weeks')
    def gen_demand_by_same_product_route_pair_without_last_2_weeks(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        adjusted_demand_s = \
            adjusted_demand_s[
                adjusted_demand_s.index.get_level_values('Semana') < 8]
        feature_s = \
            self._transformed_mean_series_multi_index_train(
                series=adjusted_demand_s,
                groupby_targets=['Producto_ID', 'Ruta_SAK'],
                feature_name='adjusted_demand_by_same_product_route_pair_without_last_2_weeks',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_product_route_pair_without_last_2_weeks': feature_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('adjusted_demand_by_same_product_client_depot_triplet_without_last_2_weeks')
    def gen_demand_by_same_product_client_depot_triplet_without_last_2_weeks(self, data):
        adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
        adjusted_demand_s = \
            adjusted_demand_s[
                adjusted_demand_s.index.get_level_values('Semana') < 8]
        feature_s = \
            self._transformed_mean_series_multi_index_train(
                series=adjusted_demand_s,
                groupby_targets=['Producto_ID', 'Cliente_ID', 'Agencia_ID'],
                feature_name='adjusted_demand_by_same_product_client_depot_triplet_without_last_2_weeks',
                standard_index=data['index'])

        return{'adjusted_demand_by_same_product_client_depot_triplet_without_last_2_weeks': feature_s}