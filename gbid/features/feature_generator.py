import os.path
from itertools import combinations

import pandas as pd
import numpy as np

from ..utils import mkdir_p, IterTimer
from .decorators import will_generate
from .registered_decorators import require, require_intermediate_data, features
from .generator_mixins import (IntermediateDataGeneratorMixin,
                               BasicInfoMixin,
                               FeaturesWithoutFutureInformationMixin,
                               Log1pMeanFeaturesWithoutFutureInfoMixin,
                               Log1pMeanFeaturesWithoutFutureInfoFillnaMixin,
                               FeaturesGroupbyFactorVariablesMixin,
                               FrequencyFeaturesMixin,
                               WeekLagFeaturesMixin)


class FeatureGenerator(IntermediateDataGeneratorMixin,
                       BasicInfoMixin,
                       FeaturesWithoutFutureInformationMixin,
                       Log1pMeanFeaturesWithoutFutureInfoMixin,
                       Log1pMeanFeaturesWithoutFutureInfoFillnaMixin,
                       FeaturesGroupbyFactorVariablesMixin,
                       FrequencyFeaturesMixin,
                       WeekLagFeaturesMixin):

    def __init__(self, data_hdf_path, feature_hdf_path):
        """
        Args:
            config: dcbcu.config.Config object
        """
        self.intermediate_data = {}
        self.data_store = pd.HDFStore(data_hdf_path)
        mkdir_p(os.path.dirname(feature_hdf_path))
        self.feature_store = pd.HDFStore(feature_hdf_path)

    def generate(self, feature_names):
        if isinstance(feature_names, str):
            feature_names = [feature_names]
        require(feature_names, self, self.intermediate_data, self.feature_store)

    def _object_encode(self, series):
        series_encode = series.copy()
        for code_num, uitem in enumerate(series.unique(), start=1):
            series_encode[series == uitem] = code_num
        return series_encode

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('label')
    def gen_label(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_transaction_df = data['train_transaction']
        label_s = train_transaction_df['Demanda_uni_equil']
        label_s.rename('label', inplace=True)
        full_label_s = label_s.reindex(data['index'], fill_value=0, copy=False)
        full_label_s.reset_index(
            level=index_to_be_removed, drop=True, inplace=True)
        assert full_label_s.isnull().sum() == 0, "label has NaN"
        return {'label': full_label_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate('log1p_label')
    def gen_log1p_label(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_transaction_df = data['train_transaction']
        label_s = train_transaction_df['Demanda_uni_equil']
        label_s.rename('log1p_label', inplace=True)
        label_s = np.log1p(label_s)
        full_label_s = label_s.reindex(data['index'], fill_value=0, copy=False)
        full_label_s.reset_index(
            level=index_to_be_removed, drop=True, inplace=True)
        return {'log1p_label': full_label_s}

##########################################################################
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate(['%s_by_same_%s' % (transcat, cat)
                    for transcat in ['sales_unit', 'sales_pesos',
                                     'returns_unit', 'returns_pesos',
                                     'adjusted_demand_unit']
                    for cat in ['depot', 'channel', 'route', 'client', 'product_id']])
    def gen_transcat_by_same_basic_cat(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        trans_df = data['train_transaction']
        basiccat_rename_dict = {
            'Agencia_ID': 'depot', 'Canal_ID': 'channel', 'Ruta_SAK': 'route',
            'Cliente_ID': 'client', 'Producto_ID': 'product_id'
        }
        transcat_rename_dict = {
            'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
            'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
            'Demanda_uni_equil': 'adjusted_demand_unit'
        }

        features = []
        for num, (old_cat, new_cat) in enumerate(basiccat_rename_dict.items()):
            print(new_cat)
            feature_name_dict = \
                {trans_cat: transcat_rename_dict[trans_cat] + '_by_same_' + new_cat
                 for trans_cat in transcat_rename_dict.keys()}

            result_df = np.round(trans_df.groupby(
                level=old_cat).agg(np.mean), 4)
            result_df.rename(columns=feature_name_dict, inplace=True)
            result_df = result_df.reindex(data['index'], level=old_cat,
                                          fill_value=0, copy=False)

            result_df.reset_index(
                level=index_to_be_removed, drop=True, inplace=True)
            features.append(result_df)

        full_df = pd.concat(features, axis=1)

        return full_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction', 'producto_table'])
    @will_generate(['%s_by_same_%s' % (transcat, cat)
                    for transcat in ['sales_unit', 'sales_pesos',
                                     'returns_unit', 'returns_pesos',
                                     'adjusted_demand_unit']
                    for cat in ['product_name', 'product_brand']])
    def gen_transcat_by_same_product_subcat(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        trans_df = data['train_transaction']
        producto_df = \
            data['producto_table']['product_name', 'product_brand']
        producto_index = \
            producto_df.set_index(
                ['product_name', 'product_brand'], append=True).index
        producto_df = \
            producto_df.reindex(
                trans_df.index, level='Producto_ID', copy=False)
        trans_extension_df = pd.concat([trans_df, producto_df], axis=1)

        product_rename_dict = {
            'product_name': 'product_name',
            'product_brand': 'product_brand'
        }
        transcat_rename_dict = {
            'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
            'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
            'Demanda_uni_equil': 'adjusted_demand_unit'
        }

        features = []
        for old_cat, new_cat in product_rename_dict.items():
            print(new_cat)
            target_columns = list(transcat_rename_dict.keys()) + [old_cat]
            target_df = trans_extension_df.loc[:, target_columns]

            feature_name_dict = \
                {trans_cat: transcat_rename_dict[trans_cat] + '_by_same_' + new_cat
                 for trans_cat in transcat_rename_dict.keys()}

            result_df = np.round(target_df.groupby(old_cat).agg(np.mean), 4)
            result_df.rename(columns=feature_name_dict, inplace=True)
            result_df = \
                result_df.reindex(producto_index, level=old_cat,
                                  fill_value=0, copy=False)
            features.append(result_df)

        full_df = pd.concat(features, axis=1)
        full_df.reset_index(
            level=['product_name', 'product_brand'], drop=True, inplace=True)
        full_df = full_df.reindex(
            data['index'], level='Producto_ID', fill_value=0, copy=False)
        full_df.reset_index(level=index_to_be_removed,
                            drop=True, inplace=True)
        return full_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction', 'agencia_table'])
    @will_generate(['%s_by_same_depot_%s' % (transcat, cat)
                    for transcat in ['sales_unit', 'sales_pesos',
                                     'returns_unit', 'returns_pesos',
                                     'adjusted_demand_unit']
                    for cat in ['town', 'state']])
    def gen_transcat_by_same_depot_subcat(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        trans_df = data['train_transaction']
        agencia_df = data['agencia_table']
        agencia_index = \
            agencia_df.set_index(['Town', 'State'], append=True).index
        agencia_df = \
            agencia_df.reindex(trans_df.index, level='Agencia_ID', copy=False)
        trans_extension_df = pd.concat([trans_df, agencia_df], axis=1)
        depot_rename_dic = {
            'Town': 'town', 'State': 'state'
        }
        transcat_rename_dict = {
            'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
            'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
            'Demanda_uni_equil': 'adjusted_demand_unit'
        }

        features = []
        for old_cat, new_cat in depot_rename_dic.items():
            target_columns = list(transcat_rename_dict.keys()) + [old_cat]
            target_df = trans_extension_df.loc[:, target_columns]

            feature_name_dict = \
                {trans_cat: transcat_rename_dict[trans_cat] + '_by_same_depot_' + new_cat
                 for trans_cat in transcat_rename_dict.keys()}

            result_df = np.round(target_df.groupby(old_cat).agg(np.mean), 4)
            result_df.rename(columns=feature_name_dict, inplace=True)
            result_df = \
                result_df.reindex(agencia_index, level=old_cat,
                                  fill_value=0, copy=False)
            features.append(result_df)

        full_df = pd.concat(features, axis=1)
        full_df.reset_index(level=['Town', 'State'], drop=True, inplace=True)
        full_df = full_df.reindex(
            data['index'], level='Agencia_ID', fill_value=0, copy=False)
        full_df.reset_index(level=index_to_be_removed, drop=True, inplace=True)

        return full_df

    ########################################################################
    # Generate features: demand + sales + returns feature  (without itself)#
    ########################################################################

    def _mean_exclude_itself(self, groupby_colname, groupby_df,
                             train_df, test_df):
        # train_transaction
        sum_df = groupby_df.agg(np.sum)
        size_s = groupby_df.size() - 1
        size_s.loc[size_s <= 0, ] = 1
        size_s = size_s.reindex(
            train_df.index, level=groupby_colname, copy=False)
        train_result_df = sum_df.reindex(train_df.index, level=groupby_colname,
                                         copy=False) - train_df
        for col in train_result_df.columns:
            train_result_df[col] = np.round(train_result_df[col] / size_s, 4)

        # test_transaction
        mean_df = np.round(groupby_df.agg(np.mean), 4)
        test_result_df = \
            mean_df.reindex(test_df.index, level=groupby_colname,
                            fill_value=0, copy=False)
        result_df = pd.concat([train_result_df, test_result_df])
        return result_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction', 'test_transaction'])
    @will_generate(['%s_by_same_%s_ex' % (transcat, cat)
                    for transcat in ['sales_unit', 'sales_pesos',
                                     'returns_unit', 'returns_pesos',
                                     'adjusted_demand_unit']
                    for cat in ['depot', 'channel', 'route', 'client', 'product_id']])
    def gen_transcat_by_same_basic_cat_ex(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_df = data['train_transaction']
        test_df = data['test_transaction']
        basiccat_rename_dict = {
            'Agencia_ID': 'depot', 'Canal_ID': 'channel', 'Ruta_SAK': 'route',
            'Cliente_ID': 'client', 'Producto_ID': 'product_id'
        }
        transcat_rename_dict = {
            'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
            'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
            'Demanda_uni_equil': 'adjusted_demand_unit'
        }

        features = []
        for num, (old_cat, new_cat) in enumerate(basiccat_rename_dict.items()):
            feature_name_dict = \
                {trans_cat: transcat_rename_dict[trans_cat] + '_by_same_' + new_cat + '_ex'
                 for trans_cat in transcat_rename_dict.keys()}

            result_df = \
                self._mean_exclude_itself(groupby_colname=old_cat,
                                          groupby_df=train_df.groupby(
                                              level=old_cat),
                                          train_df=train_df, test_df=test_df)
            result_df.rename(columns=feature_name_dict, inplace=True)
            result_df = result_df.reindex(
                data['index'], fill_value=0, copy=False)
            result_df.reset_index(
                level=index_to_be_removed, drop=True, inplace=True)
            features.append(result_df)

        full_df = pd.concat(features, axis=1)

        return full_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction',
                                'test_transaction', 'producto_table'])
    @will_generate(['%s_by_same_%s_ex' % (transcat, cat)
                    for transcat in ['sales_unit', 'sales_pesos',
                                     'returns_unit', 'returns_pesos',
                                     'adjusted_demand_unit']
                    for cat in ['product_name', 'product_brand']])
    def gen_transcat_by_same_product_subcat_ex(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        producto_df = \
            data['producto_table'].loc[:, ['product_name', 'product_brand']]
        train_extension_df = \
            pd.concat([data['train_transaction'],
                       producto_df.reindex(data['train_transaction'].index,
                                           level='Producto_ID')], axis=1)
        train_extension_df.set_index(['product_name', 'product_brand'],
                                     append=True, inplace=True)
        test_extension_df = \
            pd.concat([data['test_transaction'],
                       producto_df.reindex(data['test_transaction'].index,
                                           level='Producto_ID')], axis=1)
        test_extension_df.set_index(['product_name', 'product_brand'],
                                    append=True, inplace=True)

        product_rename_dict = {
            'product_name': 'product_name', 'product_brand': 'product_brand'
        }
        transcat_rename_dict = {
            'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
            'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
            'Demanda_uni_equil': 'adjusted_demand_unit'
        }

        features = []
        for old_cat, new_cat in product_rename_dict.items():
            print(new_cat)

            feature_name_dict = \
                {trans_cat: transcat_rename_dict[trans_cat] + '_by_same_' + new_cat + '_ex'
                 for trans_cat in transcat_rename_dict.keys()}

            result_df = \
                self._mean_exclude_itself(groupby_colname=old_cat,
                                          groupby_df=train_extension_df.groupby(
                                              level=old_cat),
                                          train_df=train_extension_df, test_df=test_extension_df)

            result_df.rename(columns=feature_name_dict, inplace=True)
            features.append(result_df)

        full_df = pd.concat(features, axis=1)
        full_df.reset_index(
            level=['product_name', 'product_brand'], drop=True, inplace=True)
        full_df = full_df.reindex(
            data['index'], level='Producto_ID', fill_value=0, copy=False)
        full_df.reset_index(level=index_to_be_removed, drop=True, inplace=True)

        return full_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction',
                                'test_transaction', 'agencia_table'])
    @will_generate(['%s_by_same_depot_%s_ex' % (transcat, cat)
                    for transcat in ['sales_unit', 'sales_pesos',
                                     'returns_unit', 'returns_pesos',
                                     'adjusted_demand_unit']
                    for cat in ['town', 'state']])
    def gen_transcat_by_same_depot_subcat_ex(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        agencia_df = data['agencia_table']
        train_extension_df = \
            pd.concat([data['train_transaction'],
                       agencia_df.reindex(data['train_transaction'].index,
                                          level='Agencia_ID')], axis=1)
        train_extension_df.set_index(
            ['Town', 'State'], append=True, inplace=True)
        test_extension_df = \
            pd.concat([data['test_transaction'],
                       agencia_df.reindex(data['test_transaction'].index,
                                          level='Agencia_ID')], axis=1)
        test_extension_df.set_index(
            ['Town', 'State'], append=True, inplace=True)

        depot_rename_dict = {
            'Town': 'town', 'State': 'state'
        }
        transcat_rename_dict = {
            'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
            'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
            'Demanda_uni_equil': 'adjusted_demand_unit'
        }

        features = []
        for old_cat, new_cat in depot_rename_dict.items():
            print(new_cat)

            feature_name_dict = \
                {trans_cat: transcat_rename_dict[trans_cat] + '_by_same_depot_' + new_cat + '_ex'
                 for trans_cat in transcat_rename_dict.keys()}

            result_df = \
                self._mean_exclude_itself(groupby_colname=old_cat,
                                          groupby_df=train_extension_df.groupby(
                                              level=old_cat),
                                          train_df=train_extension_df, test_df=test_extension_df)
            result_df.rename(columns=feature_name_dict, inplace=True)
            features.append(result_df)

        full_df = pd.concat(features, axis=1)
        full_df.reset_index(level=['Town', 'State'], drop=True, inplace=True)
        full_df = full_df.reindex(
            data['index'], level='Agencia_ID', fill_value=0, copy=False)
        full_df.reset_index(level=index_to_be_removed, drop=True, inplace=True)
        return full_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate(['rs_mean_%s_by_same_%s' % (unicat, cat)
                    for cat in ['depot', 'channel', 'route', 'client', 'product_id']
                    for unicat in ['unit', 'pesos']])
    def gen_RS_by_same_basiccat(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        trans_df = data['train_transaction']

        sales_unit_s = trans_df['Venta_uni_hoy'].copy()
        sales_pesos_s = trans_df['Venta_hoy'].copy()
        returns_unit_s = trans_df['Dev_uni_proxima'].copy()
        returns_pesos_s = trans_df['Dev_proxima'].copy()

        returns_sales_unit_ratio_s = \
            returns_unit_s.loc[sales_unit_s > 0, ] / \
            sales_unit_s.loc[sales_unit_s > 0, ]
        returns_sales_pesos_ratio_s = \
            returns_pesos_s.loc[sales_pesos_s > 0, ] / \
            sales_pesos_s.loc[sales_pesos_s > 0, ]

        basiccat_rename_dict = {
            'Agencia_ID': 'depot', 'Canal_ID': 'channel', 'Ruta_SAK': 'route',
            'Cliente_ID': 'client', 'Producto_ID': 'product_id'
        }
        features = []
        for old_cat, new_cat in basiccat_rename_dict.items():
            for item in ["unit", "pesos"]:
                feature_name = "rs_mean_" + item + "_by_same_" + new_cat
                if item == "unit":
                    mean_rs_ratio_s = \
                        returns_sales_unit_ratio_s.groupby(
                            level=old_cat).mean()
                else:
                    mean_rs_ratio_s = \
                        returns_sales_pesos_ratio_s.groupby(
                            level=old_cat).mean()
                mean_rs_ratio_s.rename(feature_name, inplace=True)
                mean_rs_ratio_s = \
                    mean_rs_ratio_s.reindex(data['index'], level=old_cat, copy=False,
                                            fill_value=np.mean(mean_rs_ratio_s))
                mean_rs_ratio_s.reset_index(level=index_to_be_removed, drop=True,
                                            inplace=True)
                features.append(mean_rs_ratio_s)

        full_df = pd.concat(features, axis=1)
        return full_df

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction'])
    @will_generate(['transformed_%s_by_same_%s' % (transcat, cat)
                    for transcat in ['sales_unit', 'sales_pesos',
                                     'returns_unit', 'returns_pesos',
                                     'adjusted_demand_unit']
                    for cat in ['depot', 'channel', 'route', 'client', 'product_id']])
    def gen_transformed_transcat_by_same_basic_cat(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        trans_df = data['train_transaction']
        basiccat_rename_dict = {
            'Agencia_ID': 'depot', 'Canal_ID': 'channel', 'Ruta_SAK': 'route',
            'Cliente_ID': 'client', 'Producto_ID': 'product_id'
        }
        transcat_rename_dict = {
            'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
            'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
            'Demanda_uni_equil': 'adjusted_demand_unit'
        }

        # transform part
        trans_df = np.log(trans_df + 1)

        features = []
        for num, (old_cat, new_cat) in enumerate(basiccat_rename_dict.items()):
            feature_name_dict = \
                {trans_cat: 'transformed_' + transcat_rename_dict[trans_cat] +
                            '_by_same_' + new_cat
                 for trans_cat in transcat_rename_dict.keys()}

            result_df = np.round(
                np.exp(trans_df.groupby(level=old_cat).agg(np.mean)), 4)
            result_df.rename(columns=feature_name_dict, inplace=True)
            result_df = result_df.reindex(data['index'], level=old_cat,
                                          fill_value=5, copy=False)
            result_df.reset_index(
                level=index_to_be_removed, drop=True, inplace=True)
            features.append(result_df)

        full_df = pd.concat(features, axis=1)

        return full_df.reset_index(drop=True)

###########################################################################
    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction',
                                'train_transaction', 'test_transaction'])
    @will_generate(['%s_demand_by_product_client_pair'
                    % cat for cat in ['median', 'transformed']])
    def gen_demand_by_product_client_pair(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_trans_df = data['train_transaction']['Demanda_uni_equil']
        trans_df = data['transaction'].reset_index()
        feature_names = []
        for cat in ['median', 'transformed']:
            feature_name = str(cat) + '_demand_by_product_client_pair'
            feature_names.append(feature_name)
            if cat == 'median':
                adjusted_demand_s = \
                    train_trans_df.groupby(
                        level=['Cliente_ID', 'Producto_ID']).median()
                adjusted_demand_s.rename(feature_name, inplace=True)
            elif cat == 'transformed':
                train_trans_df = np.log(train_trans_df + 1)
                adjusted_demand_s = \
                    train_trans_df.groupby(
                        level=['Cliente_ID', 'Producto_ID']).mean()
                adjusted_demand_s = np.exp(adjusted_demand_s) - 1

            adjusted_demand_s.rename(feature_name, inplace=True)
            adjusted_demand_s = np.round(adjusted_demand_s, 4)

            trans_df = trans_df.merge(adjusted_demand_s.reset_index(),
                                      on=['Cliente_ID', 'Producto_ID'],
                                      how='left')
        trans_df.fillna(0, inplace=True)
        trans_df.set_index(data['index'].names, inplace=True)
        trans_df = trans_df.reindex(data['index'], fill_value=0, copy=False)
        trans_df.reset_index(level=index_to_be_removed,
                             drop=True, inplace=False)

        return trans_df[feature_names]

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction',
                                'train_transaction', 'test_transaction'])
    @will_generate('transformed_demand_by_product_client_pair_ex')
    def gen_transformed_demand_by_product_client_pair_ex(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_trans_df = data['train_transaction'].reset_index()
        train_trans_df['Demanda_uni_equil'] = \
            np.log(train_trans_df['Demanda_uni_equil'] + 1)
        test_trans_df = data['test_transaction'].reset_index()
        feature_name = 'transformed_demand_by_product_client_pair_ex'
        groupby_item = train_trans_df.groupby(['Cliente_ID', 'Producto_ID'])

        sum_s = groupby_item['Demanda_uni_equil'].sum()
        sum_s.rename(feature_name, inplace=True)

        mean_s = groupby_item['Demanda_uni_equil'].mean()
        mean_s.rename(feature_name, inplace=True)

        size_s = groupby_item.size()
        size_s = size_s - 1
        size_s.loc[size_s <= 0, ] = 1
        size_s.rename('count', inplace=True)

        concat_df = pd.concat([sum_s, size_s], axis=1)

        train_trans_df = \
            train_trans_df.merge(concat_df.reset_index(),
                                 on=['Cliente_ID', 'Producto_ID'],
                                 how='left')
        train_trans_df[feature_name] = \
            (train_trans_df[feature_name] -
             train_trans_df['Demanda_uni_equil']) / train_trans_df['count']

        train_transformed_demand_s = \
            train_trans_df.set_index(data['index'].names)[feature_name]
        train_transformed_demand_s.fillna(0, inplace=True)

        test_trans_df = \
            test_trans_df.merge(mean_s.reset_index(),
                                on=['Cliente_ID', 'Producto_ID'],
                                how='left')
        test_transformed_demand_s = \
            test_trans_df.set_index(data['index'].names)[feature_name]

        test_transformed_demand_s.fillna(0, inplace=True)

        adjusted_demand_s = pd.concat([train_transformed_demand_s,
                                       test_transformed_demand_s])
        adjusted_demand_s = np.exp(adjusted_demand_s) - 1
        adjusted_demand_s = np.round(adjusted_demand_s, 4)
        adjusted_demand_s = adjusted_demand_s.reindex(data['index'],
                                                      fill_value=0, copy=False)
        adjusted_demand_s.reset_index(
            level=index_to_be_removed, drop=True, inplace=True)
        return {feature_name: adjusted_demand_s}

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'train_transaction', 'transaction'])
    @will_generate(['week_%s_median_demand_by_product_client_pair'
                    % week_number for week_number in range(3, 10)])
    def gen_median_week_demand_by_product_client_pair(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        train_trans_df = data['train_transaction']['Demanda_uni_equil']
        trans_df = data['transaction'].reset_index()
        week_numbers = list(
            set(train_trans_df.index.get_level_values('Semana')))
        feature_names = []
        for week in week_numbers:
            feature_name = 'week_' + \
                str(week) + '_median_demand_by_product_client_pair'
            week_index = train_trans_df.index.get_level_values('Semana')
            week_train_trans_df = train_trans_df[week_index == week]
            median_adjusted_demand_s = \
                week_train_trans_df.groupby(
                    level=['Cliente_ID', 'Producto_ID']).median()
            median_adjusted_demand_s.rename(feature_name, inplace=True)
            median_adjusted_demand_s = np.round(median_adjusted_demand_s, 4)
            feature_names.append(feature_name)
            trans_df = trans_df.merge(median_adjusted_demand_s.reset_index(),
                                      on=['Cliente_ID', 'Producto_ID'],
                                      how='left')
        trans_df.fillna(0, inplace=True)
        trans_df.set_index(data['index'].names, inplace=True)
        trans_df = trans_df.reindex(data['index'], fill_value=0, copy=False)
        trans_df.reset_index(level=index_to_be_removed,
                             drop=True, inplace=True)
        return trans_df[feature_names]

    @features(skip_if_exist=True)
    @require_intermediate_data(['index', 'transaction', 'train_transaction'])
    @will_generate(['week_%s_transformed_demand_by_product_client_pair'
                    % week_number for week_number in range(3, 10)])
    def gen_transformed_week_demand_by_product_client_pair(self, data):
        index_to_be_removed = list(set(data['index'].names) - set(['id']))
        trans_df = data['transaction'].reset_index()
        train_trans_df = data['train_transaction']['Demanda_uni_equil']
        train_trans_df = np.log(train_trans_df + 1)
        week_numbers = list(
            set(train_trans_df.index.get_level_values('Semana')))
        feature_names = []
        for week in week_numbers:
            feature_name = 'week_' + \
                str(week) + '_transformed_demand_by_product_client_pair'
            week_index = train_trans_df.index.get_level_values('Semana')
            week_train_trans_df = train_trans_df[week_index == week]
            transformed_adjusted_demand_s = \
                week_train_trans_df.groupby(
                    level=['Cliente_ID', 'Producto_ID']).mean()
            transformed_adjusted_demand_s = np.exp(
                transformed_adjusted_demand_s) - 1
            transformed_adjusted_demand_s.rename(feature_name, inplace=True)
            feature_names.append(feature_name)
            trans_df = trans_df.merge(transformed_adjusted_demand_s.reset_index(),
                                      on=['Cliente_ID', 'Producto_ID'],
                                      how='left')
        trans_df.fillna(0, inplace=True)
        trans_df.set_index(data['index'].names, inplace=True)
        trans_df = trans_df.reindex(data['index'], fill_value=0, copy=False)
        trans_df.reset_index(level=index_to_be_removed,
                             drop=True, inplace=True)
        return trans_df[feature_names]

    # @features(skip_if_exist=True)
    # @require_intermediate_data(['index', 'train_transaction', 'producto_table'])
    # @will_generate(['adjusted_demand_by_same_product_name',
    #                 'adjusted_demand_by_same_product_brand'])
    # def gen_demand_by_same_product_subcat(self, data):
    #     index_to_be_removed = list(set(data['index'].names) - set(['id']))
    #     producto_df = data['producto_table'][['product_name', 'product_brand']]
    #     producto_df = producto_df.reindex(data['train_transaction'].index,
    #                                       level='Producto_ID', copy=False)
    #     producto_df.set_index(['product_name', 'product_brand'], append=True)

    #     adjusted_demand_s = data['train_transaction']['Demanda_uni_equil']
    #     adjusted_demand_s.merge
    #     feature_s = \
    #         self._transformed_mean_series_single_index_train(
    #             series=adjusted_demand_s,
    #             groupby_target='Ruta_SAK',
    #             feature_name='adjusted_demand_by_same_route',
    #             standard_index=data['index'])

    #     trans_df = data['train_transaction']
    #     producto_df = \
    #         data['producto_table']['product_name', 'product_brand']
    #     producto_index = \
    #         producto_df.set_index(['product_name', 'product_brand'], append=True).index
    #     producto_df = \
    #         producto_df.reindex(trans_df.index, level='Producto_ID', copy=False)
    #     trans_extension_df = pd.concat([trans_df, producto_df], axis=1)

    #     product_rename_dict = {
    #         'product_name': 'product_name',
    #         'product_brand': 'product_brand'
    #     }
    #     transcat_rename_dict = {
    #         'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
    #         'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
    #         'Demanda_uni_equil': 'adjusted_demand_unit'
    #     }

    #     features = []
    #     for old_cat, new_cat in product_rename_dict.items():
    #         print(new_cat)
    #         target_columns = list(transcat_rename_dict.keys()) + [old_cat]
    #         target_df = trans_extension_df.loc[:, target_columns]

    #         feature_name_dict = \
    #             {trans_cat: transcat_rename_dict[trans_cat] + '_by_same_' + new_cat
    #              for trans_cat in transcat_rename_dict.keys()}

    #         result_df = np.round(target_df.groupby(old_cat).agg(np.mean), 4)
    #         result_df.rename(columns=feature_name_dict, inplace=True)
    #         result_df = \
    #             result_df.reindex(producto_index, level=old_cat,
    #                               fill_value=0, copy=False)
    #         features.append(result_df)

    #     full_df = pd.concat(features, axis=1)
    #     full_df.reset_index(level=['product_name', 'product_brand'], drop=True, inplace=True)
    #     with IterTimer("reindex"):
    #         full_df = full_df.reindex(data['index'], level='Producto_ID', fill_value=0, copy=False)
    #     with IterTimer("reset index"):
    #         full_df.reset_index(level=index_to_be_removed, drop=True, inplace=True)

    #     return full_df


##########################################################################
    # @features(skip_if_exist=True)
    # @require_intermediate_data(['index', 'train_transaction'])
    # @will_generate(['demand_by_same_trans_(%s)' % (cat[0] + ',' + cat[1])
    #                 for cat in combinations(['depot', 'channel', 'route',
    #                                          'client', 'product_id'], 2)])
    # def gen_demand_by_same_trans_bicat(self, data):
    #     basic_columns_name = data['index'].names.copy()
    #     selected_columns = basic_columns_name.copy()
    #     selected_columns.append('Demanda_uni_equil')
    #     trans_df = \
    #         data['train_transaction'].loc[:, selected_columns]

    #     old_names = ['Agencia_ID', 'Canal_ID', 'Ruta_SAK',
    #                  'Cliente_ID', 'Producto_ID']
    #     new_names = ['depot', 'channel', 'route', 'client', 'product_id']
    #     old_bicat_items = combinations(old_names, 2)
    #     new_bicat_items = combinations(new_names, 2)
    #     output_features = []
    #     for num, (old_bicat, new_bicat) in enumerate(zip(old_bicat_items, new_bicat_items)):
    #         feature_name = \
    #             'demand_by_same_trans_({})'.format(new_bicat[0] + ',' + new_bicat[1])
    #         print(feature_name)
    #         result_s = \
    #             trans_df.groupby(list(old_bicat))['Demanda_uni_equil'].mean()
    #         result_s.rename(feature_name, inplace=True)
    #         result_df = result_s.reset_index()
    #         trans_df = trans_df.merge(result_df, on=list(old_bicat),
    #                                   how='left', copy=False)
    #         output_features.append(feature_name)

    #     full_df = trans_df.reindex(data['index'], fill_value=0, copy=False)
    #     return full_df[output_features].reset_index(drop=True)

    # @features(skip_if_exist=True)
    # @require_intermediate_data(['index', 'train_transaction'])
    # @will_generate(['transformed_%s_by_same_(%s)' %(transcat, (cat[0]+ ',' + cat[1]))
    #                 for transcat in ['sales_unit', 'sales_pesos',
    #                                  'returns_unit', 'returns_pesos',
    #                                  'adjusted_demand_unit']
    #                 for cat in combinations(['depot', 'channel', 'route',
    #                                          'client', 'product_id'], 2)])
    # def gen_transformed_transcat_by_same_basic_bicat(self, data):
    #     trans_df = data['train_transaction']

    #     transcat_rename_dict = {
    #         'Venta_uni_hoy': 'sales_unit', 'Venta_hoy': 'sales_pesos',
    #         'Dev_uni_proxima': 'returns_unit', 'Dev_proxima': 'returns_pesos',
    #         'Demanda_uni_equil': 'adjusted_demand_unit'
    #     }

    #     old_names = ['Agencia_ID', 'Canal_ID', 'Ruta_SAK',
    #                  'Cliente_ID', 'Producto_ID']
    #     new_names = ['depot', 'channel', 'route', 'client', 'product_id']
    #     old_bicat_items = combinations(old_names, 2)
    #     new_bicat_items = combinations(new_names, 2)

    #     # transform part
    #     trans_df = np.log(trans_df + 1)

    #     features = []
    #     for num, (old_bicat, new_bicat) in enumerate(zip(old_bicat_items,
    #                                                      new_bicat_items)):
    #         print(new_bicat)
    #         feature_name_dict = \
    #             {trans_cat: 'transformed_' + transcat_rename_dict[trans_cat] +
    #                         '_by_same_(' + new_bicat[0] + ',' +
    #                         new_bicat[1] + ')'
    #              for trans_cat in transcat_rename_dict.keys()}

    #         result_df = \
    #             np.round(np.exp(trans_df.groupby(level=list(old_bicat)).agg(np.mean))* 0.5794, 4)
    #         result_df.rename(columns=feature_name_dict, inplace=True)
    #         result_df = result_df.reindex(data['index'], level=list(old_bicat),
    #                                       fill_value=5, copy=False)

    #         result_df = result_df.reset_index()
    #         trans_df = trans_df.merge(result_df, on=list(old_bicat),
    #                                   how='left', copy=False)
    #         full_df = pd.concat(features, axis=1)

    #     return full_df.reindex(data['index'])

    # @features(skip_if_exist=True)
    # @require_intermediate_data(['index', 'train_transaction'])
    # @will_generate(['demand_by_same_trans_%s_ex' % cat
    #                 for cat in ['depot', 'channel', 'route', 'client']])
    # def gen_unitcat_by_same_trans_cat_ex(self, data):
    #     trans_demand_s = \
    #         data['train_transaction'].loc[:, 'Demanda_uni_equil']

    #     trans_rename_dict = {
    #         'Agencia_ID': 'depot',
    #         'Canal_ID': 'channel',
    #         'Ruta_SAK': 'route',
    #         'Cliente_ID': 'client'
    #     }
    #     unitcat_rename_dict = {
    #         'Venta_uni_hoy': 'sales', 'Dev_uni_proxima': 'returns',
    #         'Demanda_uni_equil': 'adjusted_demand'
    #     }

    #     for num, (old_cat, new_cat) in enumerate(trans_rename_dict.items()):
    #         print(new_cat)
    #         feature_name = 'demand_by_same_trans_{}_ex'.format(new_cat)
    #         result_groupby = trans_demand_s.groupby(level=old_cat)
    #         result_s = self._mean_exclude_itself(self, result_groupby, trans_demand_s)
    #         result_s.rename(feature_name, inplace=True)
    #         result_s = result_s.reindex(data['index'], level=old_cat,
    #                                     fill_value=0, copy=False)
    #         if num == 0:
    #             full_df = result_s.copy()
    #         else:
    #             full_df = pd.concat([full_df, result_s], axis=1)

    #     return full_df.reset_index(drop=True)

    # @features(skip_if_exist=True)
    # @require_intermediate_data(['index', 'train_transaction', 'transaction'])
    # @will_generate(['week_%s_transformed_demand_by_product_client_pair_ex'
    #                 % week_number for week_number in range(3, 10)])
    # def gen_transformed_week_demand_by_product_client_pair_ex(self, data):
    #     trans_df = data['transaction'].reset_index()
    #     train_trans_s = data['train_transaction']['Demanda_uni_equil']
    #     train_trans_s = np.log(train_trans_s + 1)
    #     week_numbers = list(set(train_trans_s.index.get_level_values('Semana')))
    #     feature_names = []
    #     for week in week_numbers:
    #         feature_name = ('week_' + str(week) +
    #                         '_transformed_demand_by_product_client_pair_ex')
    #         feature_names.append(feature_name)

    #         week_index = train_trans_s.index.get_level_values('Semana')
    #         week_train_trans_s = train_trans_s[week_index == week]
    #         week_train_trans_df = week_train_trans_s.reset_index()

    #         adjusted_demand_s = \
    #             week_train_trans_s.groupby(level=['Cliente_ID', 'Producto_ID']).sum()
    #         size_s = \
    #             week_train_trans_s.groupby(level=['Cliente_ID', 'Producto_ID']).size() - 1
    #         size_s[size_s == 0] = 1
    #         size_s.rename('count', inplace=True)
    #         import ipdb; ipdb.set_trace()
    #         week_train_trans_df = \
    #             week_train_trans_df.merge(size_s.reset_index(),
    #                                       on=['Cliente_ID', 'Producto_ID'],
    #                                       how='left')

    #         adjusted_demand_s.rename(feature_name, inplace=True)
    #         week_train_trans_df = \
    #             week_train_trans_df.merge(adjusted_demand_s.reset_index(),
    #                                       on=['Cliente_ID', 'Producto_ID'],
    #                                       how='left')
    #         week_train_trans_df[feature_name] = \
    #             (week_train_trans_df[feature_name] -
    # week_train_trans_df['Demanda_uni_equil']) /
    # (week_train_trans_df['count'])

    #         week_train_trans_df[feature_name] = np.exp(week_train_trans_df[feature_name]) - 1
    #         transformed_adjusted_demand_s = \
    #             week_train_trans_df[['Cliente_ID', 'Producto_ID', feature_name]]

    #         import ipdb; ipdb.set_trace()
    #         trans_df = trans_df.merge(transformed_adjusted_demand_s,
    #                                   on=['Cliente_ID', 'Producto_ID'],
    #                                   how='left')
    #     trans_df.fillna(0, inplace=True)
    #     trans_df.set_index(data['index'].names, inplace=True)
    #     trans_df = trans_df.reindex(data['index'], fill_value=0, copy=False)
    #     import ipdb; ipdb.set_trace()
    #     return trans_df[feature_names]
