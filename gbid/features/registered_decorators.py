from functools import partial

from .decorators import (require, require_intermediate_data,
                         intermediate_data, features)


_FEATURE_FUNC_DICT = {}
_INTERMEDIATE_DATA_FUNC_DICT = {}
require_intermediate_data = partial(
    require_intermediate_data,
    interm_data_func_dict=_INTERMEDIATE_DATA_FUNC_DICT)
intermediate_data = partial(
    intermediate_data, func_dict=_INTERMEDIATE_DATA_FUNC_DICT)
features = partial(features, func_dict=_FEATURE_FUNC_DICT)
require = partial(require, func_dict=_FEATURE_FUNC_DICT)
