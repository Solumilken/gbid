import gc
import ipdb
from ..utils import IterTimer, mkdir_p

import pandas as pd


def sample(mode, train_user_id, test_user_id, feature_hdf_path):
    with IterTimer("Opening feature_store + user_id"):
        feature_store = pd.HDFStore(feature_hdf_path + ".h5")

    feature_label_s = feature_store['label']

    with IterTimer("Sampling dataset"):
        if mode == 'nonzero_label':
            # select label > 0
            full_nonzero_label_s = feature_label_s[feature_label_s > 0]

            train_selected_index = \
                full_nonzero_label_s.iloc[full_nonzero_label_s.index.get_level_values('USER_ID').isin(train_user_id)].index
            test_selected_part = \
                full_nonzero_label_s.iloc[full_nonzero_label_s.index.get_level_values('USER_ID').isin(test_user_id)]

            if len(test_selected_part) == 0:
                test_selected_index = \
                    feature_label_s.iloc[feature_label_s.index.get_level_values('USER_ID').isin(test_user_id)].index
            else:
                test_selected_index = test_selected_part.index

        elif mode == 'top_5_branch_visit':
            # selected top 5 branch visit
            label_groupby_id = feature_label_s.groupby(level='USER_ID')
            full_top5_label_s = \
                label_groupby_id.nlargest(5).reset_index(level=0, drop=True)
            train_selected_index = \
                full_top5_label_s.iloc[full_top5_label_s.index.get_level_values('USER_ID').isin(train_user_id)].index
            test_selected_part = \
                full_top5_label_s.iloc[full_top5_label_s.index.get_level_values('USER_ID').isin(test_user_id)]

            if sum(test_selected_part) == 0:
                test_selected_index = \
                    feature_label_s.iloc[feature_label_s.index.get_level_values('USER_ID').isin(test_user_id)].index
            else:
                test_selected_index = test_selected_part.index

        elif mode == 'none':
            train_selected_index =\
                feature_label_s.iloc[feature_label_s.index.get_level_values('USER_ID').isin(train_user_id)].index
            test_selected_index =\
                feature_label_s.iloc[feature_label_s.index.get_level_values('USER_ID').isin(test_user_id)].index

        else:
            print 'ERROR : mode is not correct.'

    selected_index = {'train_selected_index': train_selected_index,
                      'test_selected_index': test_selected_index}
    return selected_index


def load_features_with_sampling(mode, config, train_user_id, test_user_id):

    feature_hdf_path = config.feature.feature_output_path
    feature_names = config.feature.feature_list
    sampled_feature_output_path = config.feature.sampled_feature_output_path

    # Sampling
    selected_index = sample(mode, train_user_id, test_user_id, feature_hdf_path)

    with IterTimer("Opening feature_store"):
        feature_store = pd.HDFStore(feature_hdf_path + ".h5")

#    for phase in ['train', 'test']:
    for phase in ['train']:
        print phase

        features = []
        multi_index = selected_index[phase + '_selected_index']
        with IterTimer("Loading Series from " + feature_hdf_path,
                       total=len(feature_names)) as timer:
            for feature_i, key in enumerate(feature_names):
                timer.update(feature_i)
                features.append(feature_store[key][feature_store[key].index.isin(multi_index)])

        gc.collect()

        with IterTimer("Concatenating features"):
            feature_df = pd.concat(features, axis=1)

        with IterTimer("Storing feature_df"):
            if phase == 'train':
                filename = mode + '_' + phase + '_feature.h5'
                feature_df.to_hdf(sampled_feature_output_path + filename,
                                  'r_' + mode + '_' + phase + '_df')
            elif phase == 'test':
                filename = phase + '_feature.h5'
                feature_df.to_hdf(sampled_feature_output_path + filename,
                                  'r_' + phase + '_df')
