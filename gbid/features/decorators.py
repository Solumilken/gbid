from __future__ import print_function
import gc
from functools import wraps, partial

from ..utils import IterTimer


def require(keys, obj, interm_data, feature_store, func_dict):
    for key in keys:
        getattr(obj, func_dict[key])(key, interm_data, feature_store)


def can_skip(set_name, new_data_name, interm_data, feature_store,
             show_skip):
    if set_name == "intermediate_data":
        key = new_data_name
        generated_set = interm_data
    elif set_name == "features":
        key = "/" + new_data_name
        generated_set = feature_store.keys()
    else:
        raise NotImplementedError()

    if key in generated_set:
        if show_skip:
            print("skip {} {}: already generated".format(
                set_name, new_data_name))
        return True
    return False


def data_type(set_name, func_dict, skip_if_exist=True, show_skip=True):
    def intermediate_data_decorator(args):
        func, will_generate_keys = args
        if func.__name__ in func_dict.values():
            raise ValueError("duplicated function name " + func.__name__)
        for key in will_generate_keys:
            if key in func_dict:
                raise ValueError("duplicated {} {} in {} and {}".format(
                    set_name, key, func_dict[key], func.__name__))
            func_dict[key] = func.__name__

        @wraps(func)
        def func_wrapper(self, new_data_name, interm_data, feature_store,
                         *args, **kwargs):
            if skip_if_exist and can_skip(set_name, new_data_name, interm_data,
                                          feature_store, show_skip):
                return
            func(self, set_name, new_data_name, interm_data,
                 feature_store, *args, **kwargs)
            gc.collect()
        return func_wrapper
    return intermediate_data_decorator


def require_intermediate_data(data_names, interm_data_func_dict):
    if isinstance(data_names, str):
        data_names = (data_names,)

    def require_intermediate_data_decorator(args):
        func, will_generate_keys = args

        @wraps(func)
        def func_wrapper(self, set_name, new_data_name, interm_data,
                         feature_store, *args, **kwargs):
            require(data_names, self, interm_data, feature_store,
                    interm_data_func_dict)
            data = {key: self.intermediate_data[key] for key in data_names}
            func(self, set_name, new_data_name, interm_data,
                 feature_store, data, *args, **kwargs)
        return func_wrapper, will_generate_keys
    return require_intermediate_data_decorator


def will_generate(will_generate_keys):
    if isinstance(will_generate_keys, str):
        will_generate_keys = (will_generate_keys,)
    will_generate_key_set = set(will_generate_keys)

    def will_generate_decorator(func):
        @wraps(func)
        def func_wrapper(self, set_name, new_data_name, interm_data,
                         feature_store, *args, **kwargs):
            with IterTimer("Generating {} {} using {}".format(
                    set_name, new_data_name, func.__name__)):  # pylint: disable=C0330
                result_dict = func(self, *args, **kwargs)

            if not (hasattr(result_dict, 'keys')
                    and hasattr(result_dict, '__getitem__')):
                raise ValueError("the return value of mehod {} should have "
                                 "keys and __getitem__ methods".format(
                                     func.__name__))

            result_dict_key_set = set(result_dict.keys())
            if will_generate_key_set != result_dict_key_set:
                raise ValueError("the return keys of method {} {} doesn't "
                                 "match {} while generating {} {}".format(
                                     func.__name__, result_dict_key_set,
                                     will_generate_key_set, set_name,
                                     new_data_name))

            if set_name == "intermediate_data":
                interm_data.update(result_dict)
            elif set_name == "features":
                for key in result_dict_key_set:
                    if result_dict[key].isnull().any():
                        raise ValueError("features {} have nan".format(key))
                    if result_dict[key].name != key:
                        raise ValueError("dict key '{}' is not equal to "
                                         "series key '{}'"
                                         .format(key, result_dict[key].name))
                    with IterTimer("Writing generated features {} "
                                   "to hdf5 file".format(key)):
                        feature_store[key] = result_dict[key]
            else:
                raise NotImplementedError()
        return func_wrapper, will_generate_keys
    return will_generate_decorator


intermediate_data = partial(data_type, "intermediate_data")
features = partial(data_type, "features")
