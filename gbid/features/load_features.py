import gc

import pandas as pd

from ..utils import IterTimer


def load_features(feature_hdf_path, feature_names):

    feature_store = pd.HDFStore(feature_hdf_path)
    features = []

    with IterTimer("Loading and Concating Series from " + feature_hdf_path,
                   total=len(feature_names)) as timer:
        for feature_i, key in enumerate(feature_names):
            timer.update(feature_i)
            #features.append(feature_store[key])
            if feature_i == 0:
                features.append(feature_store[key])
            elif feature_i == 1:
                features.append(feature_store[key])
                feature_df = pd.concat(features, axis=1, copy=False)
            elif feature_i > 1:
                feature_df = pd.concat([feature_df, feature_store[key]], 
                                       axis=1, copy=False)
                gc.collect()

    # with IterTimer("Concating Series"):
    #     feature_df = pd.concat(features, axis=1, copy=False)
    # gc.collect()

    return feature_df
