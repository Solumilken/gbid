#Competition#
leaderboard: https://www.kaggle.com/c/grupo-bimbo-inventory-demand/leaderboard

#Build up environment#
```bash
# in the root directory of this project
python3 -m venv env
. env/bin/activate
pip install -U pip wheel && \
pip install -U cython && \
pip install -Ur requirements.txt
```

#Configuration#
- All configuration are in folder configs.
- feature1_config.yml is for generating features. (gen_features.py)
- train_config.json is for training phase.

#Pipeline#
- Preprocessing (transform csv to hdf)
```bash
./data_info_extract.py
./data_to_hdf.py 
```
- Step 1. generate features
```bash
./gen_features.py --help
```
- Step 2. train model
```bash
./train.py --help
```
- test with saved model
```bash
./test_with_model.py --help
```

#Note#
- ans_to_submission.py and random_area.py is nothing.
