#!/usr/bin/env python
from os.path import join, isfile
import pprint
import gc
from datetime import datetime
import uuid
from collections import OrderedDict

import pandas as pd
import numpy as np

from gbid.config import Config, FeatureConfig
from gbid.models.models import load_data, train_with_parameters, save_model
from gbid.models.testing import write_test_prediction
from gbid.models import gbid_predict, write_prediction
from gbid.utils import load_pkl, IterTimer, mkdir_p
from gbid.evaluations import summary


def main():
    import argparse
    import argcomplete

    parser = argparse.ArgumentParser(description='Generate features.')
    parser.add_argument('-p', '--path-config',
                        default="configs/en_path_config.yml",
                        help="the path of the path configuration yaml file")
    parser.add_argument('-f', '--feature-configs-dir',
                        default="/home/en/kaggle_gbid_2016/gbid/configs/features",
                        help="the directory of the feature configuration yaml files")
    parser.add_argument('-t', '--train-config',
                        default="configs/train_config.yml",
                        help="training configuration")
    parser.add_argument('-m', '--model-config',
                        default="configs/ensemble_model_config.yml",
                        help="model configuration")
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    config = Config(args.path_config, train_config=args.train_config,
                    model_config=args.model_config)
    model_dict = OrderedDict((
        # risky features
        ("feature_016", [
            "2016-08-27T00:40:42_07_XGBRegressor_md_10_ne_950_subsample_0.85_seed_1033",
        ]),
        ("feature_016_median", [
            "2016-08-30T17:59:10_03_Ridge_a_1e+05_rs_1033",
        ]),
        ("feature_019", [
            "2016-08-26T19:22:43_05_XGBRegressor_md_10_ne_300_subsample_0.85_seed_1033",
        ]),
        ("feature_019_median", [
            "2016-08-30T18:05:03_03_Ridge_a_1e+05_rs_1033",
        ]),
        ("feature_020", [
            "2016-08-27T22:12:33_06_XGBRegressor_md_10_ne_350_subsample_0.85_seed_1033",
        ]),

        # safe features
        # ("feature_010", [
        #     "11_GradientBoostingRegressor_md_5_ne_110_rs_1033_2016-08-16T00:22:26.256052",
        #     "2016-08-18T10:12:52_19_XGBRegressor_md_5_ne_400_subsample_0.8_seed_1033",
        # ]),
        # ("feature_012", [
        #     "2016-08-22T10:37:51_04_XGBRegressor_md_10_ne_300_subsample_0.85_seed_1033",
        # ]),
        ("feature_023_median", [
            "2016-08-30T18:13:07_03_Ridge_a_1e+05_rs_1033",
        ]),
        ("feature_023", [
            "2016-08-30T12:04:14_01_RandomForestRegressor_md_20_ne_100_rs_1033",
            "2016-08-28T20:13:03_12_XGBRegressor_md_10_ne_650_subsample_0.85_seed_1033",
            "2016-08-30T14:15:48_07_XGBRegressor_md_10_ne_400_subsample_0.85_seed_2033",
            "2016-08-29T01:01:28_09_XGBRegressor_md_10_ne_500_subsample_0.85_seed_1033",
            "2016-08-30T08:18:32_09_XGBRegressor_md_10_ne_500_subsample_0.85_seed_2033",
        ]),
        ("feature_027", [
            "2016-08-30T12:37:32_06_XGBRegressor_md_10_ne_350_subsample_0.85_seed_2033",
        ]),
        ("feature_027_median", [
            "2016-08-30T20:09:46_02_Ridge_a_1e+04_rs_1033",
        ]),
    ))
    ensemble(config, args.feature_configs_dir, model_dict)


def load_test_data(feature_config):
    scale = (feature_config.config_name in ['feature_010', 'feature_012'])
    valid_X_df, test_X_df, valid_y_df, test_y_df = load_data(
        feature_config.concat_feature_hdf_path,
        feature_config.label_column,
        feature_config.feature_list,
        [8, 9],
        [10, 11],
        scale=scale)

    valid_X = np.ascontiguousarray(valid_X_df.values)
    test_X = np.ascontiguousarray(test_X_df.values)
    valid_y = np.ascontiguousarray(valid_y_df.values)
    test_y = np.ascontiguousarray(test_y_df.values)
    valid_id = valid_y_df.index
    test_id = test_y_df.index

    valid_X_df = None
    test_X_df = None
    valid_y_df = None
    test_y_df = None
    gc.collect()

    return valid_X, test_X, valid_y, test_y, valid_id, test_id


def ensemble(config, feature_configs_dir, model_dict):  # pylint: disable=R0914,R0915
    process_started_at = datetime.now()

    model_i = 0
    new_valid_X_df = pd.DataFrame()
    new_test_X_df = pd.DataFrame()
    for feature_name, model_list in model_dict.items():
        untrained_model_list = []
        for model_name in model_list:
            valid_pred_path = join(
                config.path.prediction_output_dir,
                "valid_{}_{}.csv".format(feature_name, model_name))
            test_pred_path = join(
                config.path.prediction_output_dir,
                "test_{}_{}.csv".format(feature_name, model_name))
            if isfile(valid_pred_path) and isfile(test_pred_path):
                with IterTimer("Loading " + valid_pred_path):
                    valid_pred_df = pd.read_csv(valid_pred_path, index_col='id')
                    valid_pred_df.rename(columns={'Demanda_uni_equil': model_i},
                                         inplace=True)
                    new_valid_X_df = new_valid_X_df.merge(
                        valid_pred_df, how='outer',
                        left_index=True, right_index=True)
                with IterTimer("Loading " + test_pred_path):
                    test_pred_df = pd.read_csv(test_pred_path, index_col='id')
                    test_pred_df.rename(columns={'Demanda_uni_equil': model_i},
                                         inplace=True)
                    new_test_X_df = new_test_X_df.merge(
                        test_pred_df, how='outer',
                        left_index=True, right_index=True)
                model_i += 1
            else:
                untrained_model_list.append(model_name)
        if len(untrained_model_list) == 0:
            continue

        feature_config_path = join(feature_configs_dir, feature_name + ".yml")
        feature_config = FeatureConfig.load_from_yaml(
            config.path.feature_output_dir, feature_config_path)
        label_column = feature_config.label_column

        valid_X, test_X, valid_y, test_y, valid_id, test_id = load_test_data(
            feature_config)
        print("valid_X shape: {}".format(valid_X.shape))
        print("test_X shape : {}".format(test_X.shape))
        print("valid_y shape: {}".format(valid_y.shape))
        print("test_y shape : {}".format(test_y.shape))

        for model_name in untrained_model_list:
            valid_model_path = join(
                config.path.model_output_dir,
                "valid_{}_{}.pkl".format(feature_name, model_name))
            valid_model = load_pkl(valid_model_path, verbose=3)
            valid_pred = gbid_predict(valid_X, valid_model, label_column)
            valid_summary = summary(config.train.evaluation_metrics,
                                    valid_y, valid_pred, label_column, "valid_")
            print("valid summary:")
            pprint.pprint(valid_summary, indent=1, width=80, depth=None)

            valid_pred_s = pd.Series(valid_pred, index=valid_id)
            new_valid_X_df[model_i] = valid_pred_s

            test_model_path = join(
                config.path.model_output_dir,
                "test_{}_{}.pkl".format(feature_name, model_name))
            test_model = load_pkl(test_model_path, verbose=3)
            test_pred = gbid_predict(test_X, test_model, label_column)
            test_pred_s = pd.Series(test_pred, index=test_id)
            new_test_X_df[model_i] = test_pred_s
            model_i += 1

            valid_pred_path = join(
                config.path.prediction_output_dir,
                "valid_{}_{}.csv".format(feature_name, model_name))
            write_prediction(valid_id, valid_pred, valid_pred_path)

            test_pred_path = join(
                config.path.prediction_output_dir,
                "test_{}_{}.csv".format(feature_name, model_name))
            write_prediction(test_id, test_pred, test_pred_path)

    feature_config_path = join(feature_configs_dir, "feature_000.yml")
    feature_config = FeatureConfig.load_from_yaml(
        config.path.feature_output_dir, feature_config_path)
    config.feature = feature_config
    config.feature.config_name = "ensemble"
    valid_X, test_X, valid_y, test_y, valid_id, test_id = load_test_data(
        feature_config)
    valid_y_df = pd.Series(valid_y, index=valid_id)
    test_y_df = pd.Series(test_y, index=test_id)
    print("use feature_000 as id and label reference:")
    print("valid_X shape: {}".format(valid_X.shape))
    print("test_X shape : {}".format(test_X.shape))
    print("valid_y shape: {}".format(valid_y.shape))
    print("test_y shape : {}".format(test_y.shape))

    valid_y_df = valid_y_df.reindex(new_valid_X_df.index, copy=False)
    test_y_df = test_y_df.reindex(new_test_X_df.index, copy=False)

    valid_X = np.ascontiguousarray(new_valid_X_df.values)
    test_X = np.ascontiguousarray(new_test_X_df.values)
    valid_y = np.ascontiguousarray(valid_y_df.values)
    test_y = np.ascontiguousarray(test_y_df.values)
    print("new features:")
    print("valid_X shape: {}".format(valid_X.shape))
    print("test_X shape : {}".format(test_X.shape))
    print("valid_y shape: {}".format(valid_y.shape))
    print("test_y shape : {}".format(test_y.shape))

    valid_X = np.log1p(valid_X)
    test_X = np.log1p(test_X)

    label_column = config.feature.label_column
    result_df = pd.DataFrame()

    result_excel_path = join(
        config.path.result_output_dir,
        ("{}_{}_{}.xlsx".format(
            config.feature.config_name,
            process_started_at.strftime("%Y-%m-%dT%H:%M:%S"),
            uuid.uuid4())))
    mkdir_p(config.path.result_output_dir)
    excel_writer = pd.ExcelWriter(result_excel_path)

    for test_i, (model_class_name, model_params_name, model) in enumerate(
            train_with_parameters(config.train.model_config_path,
                                  valid_X, valid_y)):
        model_filename = save_model(
            process_started_at, test_i, model_class_name, model_params_name,
            model, config, prefix="test_")

        valid_pred = gbid_predict(valid_X, model, label_column)
        valid_summary = summary(config.train.evaluation_metrics,
                                valid_y, valid_pred, label_column, "valid_")
        test_pred = gbid_predict(test_X, model, label_column)

        result_dict = {
            'model': model_class_name,
            'params': model_params_name,
            'model_filename': model_filename,
            **valid_summary,
        }
        print("result_dict:")
        pprint.pprint(result_dict, indent=1, width=80, depth=None)

        result_df = result_df.append(result_dict, ignore_index=True)  # pylint: disable=R0204

        gc.collect()

        with IterTimer("Writing to " + excel_writer.path):
            result_df[config.train.ensemble_output_excel_columns].to_excel(
                excel_writer, "test_result")
            excel_writer.save()

        write_test_prediction(
            process_started_at, test_y_df.index, test_pred, test_i,
            model_class_name, model_params_name, config)

        # valid_pred_filename = "valid_{}_{}_{:02}_{}_{}.csv".format(
        #     config.feature.config_name,
        #     process_started_at.strftime("%Y-%m-%dT%H:%M:%S"),
        #     test_i, model_class_name, model_params_name)
        # valid_pred_path = join(config.path.prediction_output_dir, valid_pred_filename)
        # write_prediction(valid_id, valid_pred, valid_pred_path)

    # output_filename = basename(model_path)[:-4] + ".csv"
    # output_path = join(config.path.prediction_output_dir, output_filename)
    # mkdir_p(config.path.prediction_output_dir)

    # write_prediction(test_id, test_pred, output_path)

if __name__ == '__main__':
    main()
