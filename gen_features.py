#!/usr/bin/env python
import gc

import ipdb

from gbid.features import FeatureGenerator, load_features
from gbid.config import Config
from gbid.utils import IterTimer


def main():
    import argparse
    import argcomplete

    parser = argparse.ArgumentParser(description='Generate features.')
    parser.add_argument('-p', '--path-config',
                        default="configs/en_path_config.yml",
                        help="the path of the path configuration yaml file")
    parser.add_argument('-f', '--feature-config',
                        default="configs/feature1_config.yml",
                        help="the path of the feature configuration yaml file")
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    config = Config(args.path_config, args.feature_config)
    gen_features(config)


def gen_features(config):
    feature_generator = FeatureGenerator(
        config.path.data_hdf_path,
        config.feature.feature_hdf_store_path)
    columns_to_generate = (config.feature.feature_list
                           + config.feature.extra_columns
                           + [config.feature.label_column])
    import ipdb; ipdb.set_trace()
    feature_generator.generate(columns_to_generate)
    feature_generator = None
    gc.collect()

    import ipdb; ipdb.set_trace()

    concat_feature_df = load_features(
        config.feature.feature_hdf_store_path,
        columns_to_generate)
    with IterTimer("Writing to " + config.feature.concat_feature_hdf_path):
        concat_feature_df.to_hdf(config.feature.concat_feature_hdf_path,
                                 'df')

    print("Done generating features. Continue to describe DataFrame.")
    ipdb.set_trace()
    print(concat_feature_df.describe())
    ipdb.set_trace()


if __name__ == '__main__':
    main()
