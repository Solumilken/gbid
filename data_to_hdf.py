#!/usr/bin/env python
import os

import pandas as pd

from gbid.config import PathConfig
from gbid.utils import IterTimer


def main():
    import argparse
    import argcomplete

    parser = argparse.ArgumentParser(description='Generate features.')
    parser.add_argument('-p', '--path-config',
                        default="configs/en_path_config.yml",
                        help="the path of the path configuration yaml file")
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    path_config = PathConfig.load_from_yaml(args.path_config)
    data_to_hdf(path_config)


def data_to_hdf(path_config):
    if os.path.isfile(path_config.data_hdf_path):
        os.remove(path_config.data_hdf_path)
    gbid_data_store = pd.HDFStore(path_config.data_hdf_path)

    # Set index
    train_transaction_df = \
        pd.read_csv(path_config.train_transaction_path)
    test_transaction_df = pd.read_csv(path_config.test_transaction_path)
    id_max = test_transaction_df['id'].max()
    columns_as_index = list(test_transaction_df.columns)

    with IterTimer("Converting {}".format(path_config.train_transaction_path)):
        id_start = id_max + 1
        id_end = id_max + train_transaction_df.shape[0] + 1
        train_transaction_df['id'] = list(range(id_start, id_end))
        gbid_data_store['train_transaction'] = \
            train_transaction_df.set_index(columns_as_index)

    with IterTimer("Converting {}".format(path_config.test_transaction_path)):
        gbid_data_store['test_transaction'] = \
            test_transaction_df.set_index(columns_as_index)

    with IterTimer("Converting {}".format(path_config.client_table_path)):
        client_table_df = pd.read_csv(path_config.client_table_path)
        gbid_data_store['cliente_table_orig'] = \
            client_table_df.set_index('Cliente_ID')

    with IterTimer("Converting {}".format(path_config.client_table_extension_path)):
        client_table_extension_df = \
            pd.read_hdf(path_config.client_table_extension_path, 'client')
        gbid_data_store['client_table'] = \
            client_table_extension_df.set_index('Cliente_ID')

    with IterTimer("Converting {}".format(path_config.product_table_path)):
        product_table_df = \
            pd.read_csv(path_config.product_table_path)
        gbid_data_store['producto_table_orig'] = \
            product_table_df.set_index('Producto_ID')

    with IterTimer("Converting {}".format(path_config.product_table_extension_path)):
        product_table_extension_df = \
            pd.read_hdf(path_config.product_table_extension_path, 'product')
        gbid_data_store['producto_table'] = \
            product_table_extension_df.set_index('Producto_ID')

    with IterTimer("Converting {}".format(path_config.sales_depot_table_path)):
        sales_depot_table_df = \
            pd.read_csv(path_config.sales_depot_table_path)
        gbid_data_store['agencia_table'] = \
            sales_depot_table_df.set_index('Agencia_ID')

    with IterTimer("Combining transaction"):
        train_transaction_df_index = train_transaction_df[columns_as_index]
        test_transaction_df_index = test_transaction_df[columns_as_index]
        assert (train_transaction_df_index.columns == test_transaction_df_index.columns).all()
        gbid_data_store['transaction'] = \
            pd.concat([train_transaction_df_index,
                       test_transaction_df_index]).set_index(columns_as_index)

if __name__ == '__main__':
    main()
