#!/usr/bin/env python
import uuid
from os.path import join
from datetime import datetime

import pandas as pd

from gbid.config import Config
from gbid.models import validate, test
from gbid.utils import IterTimer, mkdir_p


def main():
    import argparse
    import argcomplete

    parser = argparse.ArgumentParser(description='Generate features.')
    parser.add_argument('-p', '--path-config',
                        default="configs/en_path_config.yml",
                        help="the path of the path configuration yaml file")
    parser.add_argument('-f', '--feature-config',
                        default="configs/feature1_config.yml",
                        help="the path of the feature configuration yaml file")
    parser.add_argument('-t', '--train-config',
                        default="configs/train_config.yml",
                        help="training configuration")
    parser.add_argument('-m', '--model-config',
                        default="configs/model_config.yml",
                        help="model configuration")
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    config = Config(args.path_config, args.feature_config, args.train_config,
                    args.model_config)
    train(config)


def train(config):
    process_started_at = datetime.now()

    result_excel_path = join(
        config.path.result_output_dir,
        ("{}_{}_{}.xlsx".format(
            config.feature.config_name,
            process_started_at.strftime("%Y-%m-%dT%H:%M:%S"),
            uuid.uuid4())))
    mkdir_p(config.path.result_output_dir)
    excel_writer = pd.ExcelWriter(result_excel_path)

    with IterTimer("Validating", verbose=2):
        result_df = validate(config, process_started_at, excel_writer)
    print(result_df)

    with IterTimer("Testing", verbose=2):
        result_df = test(config, process_started_at, excel_writer)
    print(result_df)


if __name__ == '__main__':
    main()
