#!/usr/bin/env python
from os.path import join, basename
import pprint

import pandas as pd

from gbid.config import Config
from gbid.models.testing import load_test_data
from gbid.models import write_prediction, gbid_predict
from gbid.utils import load_pkl, mkdir_p
from gbid.evaluations import summary


def main():
    import argparse
    import argcomplete

    parser = argparse.ArgumentParser(description='Generate features.')
    parser.add_argument('model',
                        help="the .pkl model file")
    parser.add_argument('-p', '--path-config',
                        default="configs/en_path_config.yml",
                        help="the path of the path configuration yaml file")
    parser.add_argument('-f', '--feature-config',
                        default="configs/feature1_config.yml",
                        help="the path of the feature configuration yaml file")
    parser.add_argument('-t', '--train-config',
                        default="configs/train_config.yml",
                        help="training configuration")
    parser.add_argument('-m', '--model-config',
                        default="configs/model_config.yml",
                        help="model configuration")
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    config = Config(args.path_config, args.feature_config, args.train_config,
                    args.model_config)
    train_with_model(args.model, config)


def train_with_model(model_path, config):
    model = load_pkl(model_path, verbose=3)
    if hasattr(model, 'feature_importances_'):
        imp_s = pd.Series(model.feature_importances_,
                          index=config.feature.feature_list,
                          name='feature_importances')
        imp_s.sort_values(ascending=False, inplace=True)
        print(imp_s)

    train_X, test_X, train_y, test_y, test_id = load_test_data(config)

    print("train_X shape: {}".format(train_X.shape))
    print("test_X shape : {}".format(test_X.shape))
    print("train_y shape: {}".format(train_y.shape))
    print("test_y shape : {}".format(test_y.shape))

    label_column = config.feature.label_column

    train_pred = gbid_predict(train_X, model, label_column)
    train_summary = summary(config.train.evaluation_metrics,
                            train_y, train_pred, label_column, "train_")
    print("train summary:")
    pprint.pprint(train_summary, indent=1, width=80, depth=None)

    test_pred = gbid_predict(test_X, model, label_column)

    output_filename = basename(model_path)[:-4] + ".csv"
    output_path = join(config.path.prediction_output_dir, output_filename)
    mkdir_p(config.path.prediction_output_dir)

    write_prediction(test_id, test_pred, output_path)

if __name__ == '__main__':
    main()
