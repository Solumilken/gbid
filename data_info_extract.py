#!/usr/bin/env python
import re

import pandas as pd
import numpy as np

from gbid.config import PathConfig
from gbid.utils import IterTimer


def main():
    import argparse
    import argcomplete

    parser = argparse.ArgumentParser(description='Generate features.')
    parser.add_argument('-p', '--path-config',
                        default="configs/en_path_config.yml",
                        help="the path of the path configuration yaml file")
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    path_config = PathConfig.load_from_yaml(args.path_config)
    product_info_extract(path_config)
    client_info_extract(path_config)
    print("done.")

def product_info_extract(path_config):
    with IterTimer("Loading {}".format(path_config.product_table_path)):
        producto_tabla_df = pd.read_csv(path_config.product_table_path)
        product_name_s = producto_tabla_df.NombreProducto

    with IterTimer("Spliting and Extending {}".format(path_config.product_table_path)):
        # product name
        producto_tabla_df['product_name'] = \
            product_name_s.str.extract('^(\D*)', expand=False)

        # product product_brand
        producto_tabla_df['product_brand'] = \
            product_name_s.str.extract('^.+\s(\D+) \d+$', expand=False).fillna('Unknown')

        # product product_weight
        w = product_name_s.str.extract('(\d+)(Kg|kg|g|G|ml)', expand=True)
        producto_tabla_df['product_weight'] = \
            w[0].astype('float') * w[1].map({'Kg': 1000, 'kg': 1000, 'g': 1,
                                             'G': 1, 'ml': 0.9})
        producto_tabla_df['product_weight'].fillna(producto_tabla_df['product_weight'].median(),
                                                   inplace=True)
        producto_tabla_df['product_weight'] = \
            producto_tabla_df['product_weight'].astype(np.int, copy=False)

        # product product_product_volume
        producto_tabla_df['product_volume'] = \
            product_name_s.str.extract('(\d+)ml', expand=False).fillna(0)
        producto_tabla_df['product_volume'] = \
            producto_tabla_df['product_volume'].astype(np.int, copy=False)

        # product product_pieces
        p = product_name_s.str.extract('(\d+)(p|P)\s', expand=True)
        producto_tabla_df['product_pieces'] = \
            p[0].astype('float') * p[1].map({'P': 1, 'p': 1})
        producto_tabla_df['product_pieces'].fillna(1, inplace=True)
        producto_tabla_df['product_pieces'] = producto_tabla_df['product_pieces'].astype(np.int)

    with IterTimer("Storing {}".format(path_config.product_table_extension_path)):
        producto_tabla_df.to_hdf(path_config.product_table_extension_path, 'product')


def client_info_extract(path_config):
    with IterTimer("Loading {}".format(path_config.client_table_path)):
        cliente_tabla_df = pd.read_csv(path_config.client_table_path)
        clear_names = []
    with IterTimer("Extracting client info"):
        for row, name in enumerate(cliente_tabla_df['NombreCliente']):
            new_name = re.sub('\s+', ' ', name)
            clear_names.append(new_name)
        cliente_tabla_df['client_name_clear'] = clear_names

    with IterTimer("Storing {}".format(path_config.client_table_path)):
        cliente_tabla_df.to_hdf(path_config.client_table_extension_path, 'client')

if __name__ == '__main__':
    main()
